﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WebApplication1.sales
{
    public partial class frmYEOA_M_Master : System.Web.UI.Page
    {
        
        SqlConnection constr = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnectionString"].ToString());
        SqlDataAdapter adapt;
        DataTable dt;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                showData();

            }
        }

        protected void rgvPremiumFreight_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
        {
            rgvPremiumFreight.EditIndex = -1;
            showData();
        }

        protected void rgvPremiumFreight_RowEditing(object sender, GridViewEditEventArgs e)
        {
            rgvPremiumFreight.EditIndex = e.NewEditIndex;
            showData();
        }

        protected void rgvPremiumFreight_RowUpdating(object sender, GridViewUpdateEventArgs e)
        {
            constr.Open();
            try
            {

                TextBox plant = rgvPremiumFreight.Rows[e.RowIndex].FindControl("txt_plant") as TextBox;
                TextBox Air = rgvPremiumFreight.Rows[e.RowIndex].FindControl("txt_Air") as TextBox;
                SqlCommand cmd = new SqlCommand("Update [tblYEO_A_M] set YEO='" + Air.Text + "' where Plant='" + plant.Text + "'", constr);
                cmd.ExecuteNonQuery();
                rgvPremiumFreight.EditIndex = -1;
                constr.Close();
                showData();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                constr.Close();
            }
        }

        protected void showData()
        {
            try
            {
                dt = new DataTable();
                constr.Open();
                adapt = new SqlDataAdapter("select * from [tblYEO_A_M]", constr);
                adapt.Fill(dt);
                if (dt.Rows.Count > 0)
                {
                    rgvPremiumFreight.DataSource = dt;
                    rgvPremiumFreight.DataBind();
                }

            }
            catch (Exception ex) { throw ex; }
            finally
            {
                constr.Close();
            }


        }
    }
}