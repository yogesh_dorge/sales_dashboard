﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Configuration;
using Telerik.Web.UI;
using Telerik;
using System.Data;
using System.IO;
using System.Data.OleDb;
using System.Globalization;
using System.Text;

namespace WebApplication1.sales
{
    public partial class frmHolidayMaster : System.Web.UI.Page
    {
        SqlConnection constr = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnectionString"].ToString());
        SqlDataAdapter adapt;
        DataTable dt;
        OleDbConnection Econ;
        SqlConnection con;
        string constrNew, Query, sqlconn, sql;
        string projectId = string.Empty;
        string Role = "";
        string projectPath = "";
        string activityPath = "";
        string qry = "";
        SqlCommand cmd;
        string stagecode = "";


        string month, year;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                // ShowData();

                month = ddlMonth.SelectedValue;
                year = ddlYear.SelectedItem.Text;

                showGrid(month, year);
            }
            else {

                month = ddlMonth.SelectedValue;
                year = ddlYear.SelectedItem.Text;
               
            
            }
         
        }


        private void ShowData()
        {
            try
            {
                dt = new DataTable();
                constr.Open();
                //adapt = new SqlDataAdapter("select Ydate,Day,IsHoliday from tblHolidays where Ydate between (select CONVERT(DATE,dateadd(dd,-(day(getdate())-1),getdate()))) and (select dateadd(s,-1,dateadd(mm,datediff(m,0,getdate())+1,0)))", constr);

                adapt = new SqlDataAdapter("SELECT [YDate],[Day] ,[IsHoliday] FROM [dbSale_Live].[dbo].[tblHolidays] Order by case when [YDate]=CONVERT(char(10), DATEADD(month, DATEDIFF(MONTH,0,GETDATE() ), 0),126) then 1 else 2 end", constr);
                adapt.Fill(dt);
                if (dt.Rows.Count > 0)
                {
                    GridView1.DataSource = dt;
                    GridView1.DataBind();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                constr.Close();
            }
        }

        protected void GridView1_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
        {
            GridView1.EditIndex = -1;
          //  ShowData();
            showGrid(month, year);
        }

        protected void GridView1_RowEditing(object sender, GridViewEditEventArgs e)
        {
            GridView1.EditIndex = e.NewEditIndex;
           // ShowData();
            showGrid(month, year);
        }

        protected void GridView1_RowUpdating(object sender, GridViewUpdateEventArgs e)
        {
            constr.Open();
            try
            {

                Label date = GridView1.Rows[e.RowIndex].FindControl("lbl_Id") as Label;
                TextBox Day = GridView1.Rows[e.RowIndex].FindControl("Day") as TextBox;
                TextBox Isholiday = GridView1.Rows[e.RowIndex].FindControl("txt_Target") as TextBox;

                SqlCommand cmd = new SqlCommand("Update tblHolidays set IsHoliday='" + Isholiday.Text + "' where Ydate='" + date.Text + "'", constr);
                cmd.ExecuteNonQuery();
                GridView1.EditIndex = -1;
                constr.Close();
                //ShowData();
                showGrid(month, year);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                constr.Close();
            }
        }

        protected void btnUpload_Click(object sender, EventArgs e)
        {
            string fileName = FileUpload1.FileName;
            fnDeleteFile(fileName);


            if (FileUpload1.HasFile)
            {
                string[] validFileTypes = { "csv" };
                string ext = System.IO.Path.GetExtension(FileUpload1.PostedFile.FileName);

                bool isValidFile = false;

                for (int i = 0; i < validFileTypes.Length; i++)
                {

                    if (ext == "." + validFileTypes[i])
                    {

                        isValidFile = true;

                        break;

                    }

                }

                if (!isValidFile)
                {
                    System.Web.UI.ScriptManager.RegisterStartupScript(this, this.GetType(), "alert", "alert('Invalid File. Please upload a File with extension .CSV' );window.location ='frmHolidayMaster.aspx';", true);

                  


                    Label1.ForeColor = System.Drawing.Color.Red;

                    Label1.Text = "Invalid File. Please upload a File with extension " +

                                   string.Join(",", validFileTypes);

                }

                else
                {


                

                    //Creating object of datatable  
                    DataTable tb = new DataTable();
                    tb.Columns.Add("YDate");
                    tb.Columns.Add("Day");
                    tb.Columns.Add("IsHoliday");

                    string fileNameToSave = Path.GetFileName(FileUpload1.PostedFile.FileName);


                    FileUpload1.PostedFile.SaveAs(Server.MapPath("~/File/") + fileNameToSave);


                    string CurrentFilePath = Server.MapPath("~/File/" + FileUpload1.PostedFile.FileName);
                    //Reading All text  
                    try
                    {
                        string ReadCSV = File.ReadAllText(CurrentFilePath);
                        //spliting row after new line  
                        foreach (string csvRow in ReadCSV.Split('\n'))
                        {
                            if (!string.IsNullOrEmpty(csvRow))
                            {
                                //Adding each row into datatable  
                                tb.Rows.Add();
                                int count = 0;
                                foreach (string FileRec in csvRow.Split(','))
                                {
                                    tb.Rows[tb.Rows.Count - 1][count] = FileRec;
                                    count++;
                                }
                            }


                        }
                    }
                    catch (Exception ex)
                    {
                        System.Web.UI.ScriptManager.RegisterStartupScript(this, this.GetType(), "alert", "alert('Invalid Format please check demo');window.location ='frmHolidayMaster.aspx';", true);

                        Label1.Text = "This is not valid Format";
                    }
                    //Calling insert Functions  
                   
                   string result= InsertCSVRecords(tb);

                   if (result == "ok")
                   {


                       fnDeleteFile(fileName);

                       System.Web.UI.ScriptManager.RegisterStartupScript(this, this.GetType(), "alert", "alert('File Updated Sucessfully');window.location ='frmHolidayMaster.aspx';", true);
                     
                       Label1.ForeColor = System.Drawing.Color.Green;

                       Label1.Text = "File uploaded successfully.";
                   }
                   else {
                       System.Web.UI.ScriptManager.RegisterStartupScript(this, this.GetType(), "alert", "alert('Please upload proper format or Check Demo file');window.location ='frmHolidayMaster.aspx';", true);
                       Label1.Text = "Please upload proper format or Check Demo file";
                   }
                }
            }
            else
            {
                System.Web.UI.ScriptManager.RegisterStartupScript(this, this.GetType(), "alert", "alert('Please Select File');window.location ='frmHolidayMaster.aspx';", true);

                //Label1.Text = "Please Select File";

            }
        }


        //new Method



        private void connection()
        {
            sql = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            connectionObj = new SqlConnection(sql);
        }
        SqlConnection connectionObj;

        private void fnDeleteFile(string file_name)
        {
            try
            {
                string path = Server.MapPath("~/File/" + file_name);
                FileInfo file = new FileInfo(path);
                if (file.Exists)//check file exsit or not
                {
                    file.Delete();

                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }





        private string InsertCSVRecords(DataTable csvdt)
        {
            try
            {
                //csvdt.Columns[1].DataType = typeof(DateTime);
                connection();
                connectionObj.Open();
                SqlTransaction transaction = connectionObj.BeginTransaction();
                //creating object of SqlBulkCopy    
                SqlBulkCopy objbulk = new SqlBulkCopy(connectionObj, SqlBulkCopyOptions.KeepIdentity, transaction);
                //assigning Destination table name    
                objbulk.DestinationTableName = "tblHolidays";
                //Mapping Table column    
                objbulk.ColumnMappings.Add("YDate", "YDate");
                objbulk.ColumnMappings.Add("Day", "Day");
                objbulk.ColumnMappings.Add("IsHoliday", "IsHoliday");
                //objbulk.ColumnMappings.Add("Designation", "Designation");

                //inserting Datatable Records to DataBase    
                //connectionObj.Open();


                objbulk.WriteToServer(csvdt);
                transaction.Commit();

                //qry = "WITH CTE AS(SELECT *,R=RANK() OVER (ORDER BY Holiday_Date) FROM tblMasterHoliday) DELETE CTE WHERE R IN (SELECT R FROM CTE GROUP BY R HAVING COUNT(*)>1)";
                qry = "WITH CTE AS(   SELECT YDate,       RN = ROW_NUMBER() OVER(PARTITION BY YDate ORDER BY YDate)   FROM [tblHolidays]) DELETE FROM CTE WHERE RN > 1";
                DBUtils.ExecuteSQLCommand(new SqlCommand(qry));

                connectionObj.Close();
                return "ok";
            }
            catch (Exception ex)
            {
                return "NotOk";
               
            }
        }

        protected void GridView1_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            GridView1.PageIndex = e.NewPageIndex;
            showGrid(month, year);
           // ShowData();
           // GridView1.DataBind();
        }

        protected void btnSampleCsv_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/File/SampleCSV.csv");
        }

        protected void btnExportToCsv_Click(object sender, EventArgs e)
        {
            //ExportGridToCsv();
            fnDtToCsv();
        }

        private void ExportGridToCsv()
        {
            Response.Clear();

            Response.Buffer = true;

            Response.AddHeader("content-disposition",

             "attachment;filename=HolidayMaster.csv");

            Response.Charset = "";

            Response.ContentType = "application/text";



            GridView1.AllowPaging = false;

            GridView1.DataBind();



            StringBuilder sb = new StringBuilder();

            for (int k = 0; k < GridView1.Columns.Count; k++)
            {

                //add separator

                sb.Append(GridView1.Columns[k].HeaderText + ',');

            }

            //append new line

            sb.Append("\r\n");

            for (int i = 0; i < GridView1.Rows.Count; i++)
            {

                for (int k = 0; k < GridView1.Columns.Count; k++)
                {

                    //add separator

                    sb.Append(GridView1.Rows[i].Cells[k].Text + ',');

                }

                //append new line

                sb.Append("\r\n");

            }

            Response.Output.Write(sb.ToString());

            Response.Flush();

            Response.End();


        }
        private void ExportGridToExcel()
        {
            Response.Clear();
            Response.AddHeader("content-disposition", "attachment;filename=HolidayMaster.xls");
            Response.ContentType = "File/Data.xls";
            StringWriter StringWriter = new System.IO.StringWriter();
            HtmlTextWriter HtmlTextWriter = new HtmlTextWriter(StringWriter);

            GridView1.RenderControl(HtmlTextWriter);
            Response.Write(StringWriter.ToString());
            Response.End();
        }

        public override void VerifyRenderingInServerForm(Control control)
        {
            /* Verifies that the control is rendered */
        }

        protected void btnExportExcel_Click(object sender, EventArgs e)
        {
            ExportGridToExcel();
        }

        private void fnDtToCsv() {
            try
            {
        
              string qry = "select Ydate,Day,IsHoliday from tblHolidays";
            DataTable dt = DBUtils.SQLSelect(new SqlCommand(qry));


         string csv = string.Empty;

                            foreach (DataColumn column in dt.Columns)
                            {
                                //Add the Header row for CSV file.
                                csv += column.ColumnName + ',';
                            }

                            //Add new line.
                            csv += "\r\n";

                            foreach (DataRow row in dt.Rows)
                            {
                                foreach (DataColumn column in dt.Columns)
                                {
                                    //Add the Data rows.
                                    csv += row[column.ColumnName].ToString().Replace(",", ";") + ',';
                                }

                                //Add new line.
                                csv += "\r\n";
                            }
                                Response.Clear();
                                Response.Buffer = true;
                                Response.AddHeader("content-disposition", "attachment;filename=Holiday.csv");
                                Response.Charset = "";
                                Response.ContentType = "application/text";
                                Response.Output.Write(csv);
                                Response.Flush();
                                Response.End();
                          


            }
            catch (Exception ex) {
                throw ex;
            
            }
        
        
        
        }

        protected void ddlMonth_SelectedIndexChanged(object sender, EventArgs e)
        {
            showGrid(month, year);
        }

        protected void ddlYear_SelectedIndexChanged(object sender, EventArgs e)
        {
            showGrid(month, year);
        }
        private void showGrid(string month,string year) {

           


            string q = "select [YDate],[Day] ,[IsHoliday]  from tblHolidays where MONTH(Ydate)='" + month + "' and YEAR(Ydate)='" + year + "' order by DAY(YDate) asc";

            DataTable dt= DBUtils.SQLSelect(new SqlCommand(q));

                GridView1.DataSource = dt;
                GridView1.DataBind();
            
        
        
        }

      
    



    }
}
























