﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using Telerik.Web.UI;
using Telerik;
using System.Web.Security;
using System.Web.Configuration;
using System.Net.Mail;
using System.Net;
using System.Web.SessionState;
using System.Globalization;
using System.Text.RegularExpressions;
using unirest_net.http;

namespace WebApplication1
{

    public partial class SaleDashboard : System.Web.UI.Page
    {
        String Monthly = string.Empty;
        String Apr = string.Empty;
        Int32 i = 0;
        string count = string.Empty;
        string Query1 = "";
        string Query2 = "";
        string PlantQuery = String.Empty;
        Dictionary<string, Int16> MonthList = new Dictionary<string, Int16>();
        SqlConnection constr = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnectionString"].ToString());
        string month = string.Empty;

        public static DataTable GetInversedDataTable(DataTable table, string columnX, params string[] columnsToIgnore)
        {
            //Create a DataTable to Return
            DataTable returnTable = new DataTable();

            if (columnX == "")
                columnX = table.Columns[0].ColumnName;

            //Add a Column at the beginning of the table

            returnTable.Columns.Add(columnX);

            //Read all DISTINCT values from columnX Column in the provided DataTale
            List<string> columnXValues = new List<string>();

            //Creates list of columns to ignore
            List<string> listColumnsToIgnore = new List<string>();
            if (columnsToIgnore.Length > 0)
                listColumnsToIgnore.AddRange(columnsToIgnore);

            if (!listColumnsToIgnore.Contains(columnX))
                listColumnsToIgnore.Add(columnX);

            foreach (DataRow dr in table.Rows)
            {
                string columnXTemp = dr[columnX].ToString();
                //Verify if the value was already listed
                if (!columnXValues.Contains(columnXTemp))
                {
                    //if the value id different from others provided, add to the list of 
                    //values and creates a new Column with its value.
                    columnXValues.Add(columnXTemp);
                    returnTable.Columns.Add(columnXTemp);
                }
                else
                {
                    //Throw exception for a repeated value
                    throw new Exception("The inversion used must have " +
                                        "unique values for column " + columnX);
                }
            }

            //Add a line for each column of the DataTable

            foreach (DataColumn dc in table.Columns)
            {
                if (!columnXValues.Contains(dc.ColumnName) &&
                    !listColumnsToIgnore.Contains(dc.ColumnName))
                {
                    DataRow dr = returnTable.NewRow();
                    dr[0] = dc.ColumnName;
                    returnTable.Rows.Add(dr);
                }
            }

            //Complete the datatable with the values
            for (int i = 0; i < returnTable.Rows.Count; i++)
            {
                for (int j = 1; j < returnTable.Columns.Count; j++)
                {
                    returnTable.Rows[i][j] =
                      table.Rows[j - 1][returnTable.Rows[i][0].ToString()].ToString();
                }
            }
            return returnTable;
        }

        private void LoadIfHoliday()
        {
            string date = "";
            try
            {
                string Query = "select * from YesterdayHolidaySum";
                SqlCommand cmd = new SqlCommand(Query, constr);
                constr.Open();
                SqlDataReader dr = cmd.ExecuteReader();
                if (dr.Read())
                {
                    date = dr.GetValue(0).ToString();
                }

                if (date == "")
                {
                    Query1 = "CAST(dbo.Final_Yest.Y_Sale/ 100000 AS decimal(10, 2))";
                    Query2 = "Col13";
                }
                else
                {
                    Query1 = "cast(dbo.Final_Yest.Y_Sale/ 100000+dbo.Final_Yest.Y_1_Sale/ 100000 as numeric(18,2))";
                    Query2 = "CAST(Col13 as numeric(18,0))+CAST(Col16 as numeric(18,0))";
                }
            }
            catch (Exception ex)
            {
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alertMessage", "alert(" + ex.Message + ")", true);
            }
            finally
            {
                constr.Close();
            }
        }

        private void ReturnMonth()
        {
            MonthList.Add("Jan", 3);
            MonthList.Add("Feb", 2);
            MonthList.Add("Mar", 1);
            MonthList.Add("Apr", 12);
            MonthList.Add("May", 11);
            MonthList.Add("Jun", 10);
            MonthList.Add("Jul", 9);
            MonthList.Add("Aug", 8);
            MonthList.Add("Sep", 7);
            MonthList.Add("Oct", 6);
            MonthList.Add("Nov", 5);
            MonthList.Add("Dec", 4);

            string dateNow = "";
            month = DateTime.Now.ToString("MMM");
            dateNow = DateTime.Now.ToShortDateString();
            if (month == "Nov")
            {
                if (MonthList.ContainsKey("Nov") == true)
                {
                    count = MonthList["Nov"].ToString();
                }
                PlantQuery = "SELECT tblPlantMaster.plantName AS Plant, SaleMaster_15_16.sale AS [FY 15-16], SaleMaster_16_17.sale AS [FY 16-17],SaleMaster_17_18.sale As [FY 17-18], Avgmnth_17_18.sale As [Avg/Mnth 17-18], tblBudgetMaster.Budget AS [Bud FY 18-19],CAST(tblBudgetMaster.Budget/12 as numeric(18,2)) as [Monthly Bud], EveryMonth.April, EveryMonth.May, EveryMonth.June, EveryMonth.July, EveryMonth.August,EveryMonth.September,tblRevYeo.YEO as [Rev YEO],tblYEO_A_M.YEO as [YEO Avg/Mnth],EveryMonth.October,EveryMonth.November,cast((CAST(tblRevYeo.YEO as decimal(18,2)) -Cast(April+May+June+July+August+September+October as numeric(18,2)))/" + Convert.ToInt32(count) + " as numeric(18,2))as [Asking Rate Nov],(April+May+June+July+August+September+October+November) as [YTD Sale]," + Query1 + " as [Yest Sale (in Lakh)],CAST(Avg_Sale.AvgSale/(select * from DaysCount) as numeric(18,2))AS [Avg/Day(in lac)] FROM         tblPlantMaster INNER JOIN EveryMonth ON tblPlantMaster.plantName = EveryMonth.Plant INNER JOIN SaleMaster_16_17 ON tblPlantMaster.plantName = SaleMaster_16_17.Plant INNER JOIN SaleMaster_15_16 ON tblPlantMaster.plantName = SaleMaster_15_16.Plant INNER JOIN tblBudgetMaster ON tblPlantMaster.plantName = tblBudgetMaster.Plant INNER JOIN Final_Yest ON tblPlantMaster.plantName=Final_Yest.PlantGroup INNER JOIN Avg_Sale ON tblPlantMaster.plantName=Avg_Sale.Plant INNER JOIN tblRevYeo on tblPlantMaster.plantName=tblRevYeo.Plant INNER JOIN tblYEO_A_M on tblPlantMaster.plantName=tblYEO_A_M.Plant INNER JOIN SaleMaster_17_18 ON tblPlantMaster.plantName=SaleMaster_17_18.Plant INNER JOIN Avgmnth_17_18 ON tblPlantMaster.plantName=Avgmnth_17_18.Plant";
                
            }
            else if (month == "Dec")
            {
                if (MonthList.ContainsKey("Dec") == true)
                {
                    count = MonthList["Dec"].ToString();
                }
                PlantQuery = "SELECT tblPlantMaster.plantName AS Plant, SaleMaster_15_16.sale AS [FY 15-16], SaleMaster_16_17.sale AS [FY 16-17],SaleMaster_17_18.sale As [FY 17-18], Avgmnth_17_18.sale As [Avg/Mnth 17-18], tblBudgetMaster.Budget AS [Bud FY 18-19],CAST(tblBudgetMaster.Budget/12 as numeric(18,2)) as [Monthly Bud], EveryMonth.April, EveryMonth.May, EveryMonth.June, EveryMonth.July, EveryMonth.August,EveryMonth.September,tblRevYeo.YEO as [Rev YEO],tblYEO_A_M.YEO as [YEO Avg/Mnth],EveryMonth.October,EveryMonth.November,EveryMonth.December,cast((CAST(tblRevYeo.YEO as decimal(18,2)) -Cast(April+May+June+July+August+September+October+November as numeric(18,2)))/" + Convert.ToInt32(count) + " as numeric(18,2))as [Asking Rate Dec],(April+May+June+July+August+September+October+November+December) as [YTD Sale]," + Query1 + " as [Yest Sale (in Lakh)],CAST(Avg_Sale.AvgSale/(select * from DaysCount) as numeric(18,2))AS [Avg/Day(in lac)] FROM         tblPlantMaster INNER JOIN EveryMonth ON tblPlantMaster.plantName = EveryMonth.Plant INNER JOIN SaleMaster_16_17 ON tblPlantMaster.plantName = SaleMaster_16_17.Plant INNER JOIN SaleMaster_15_16 ON tblPlantMaster.plantName = SaleMaster_15_16.Plant INNER JOIN tblBudgetMaster ON tblPlantMaster.plantName = tblBudgetMaster.Plant INNER JOIN Final_Yest ON tblPlantMaster.plantName=Final_Yest.PlantGroup INNER JOIN Avg_Sale ON tblPlantMaster.plantName=Avg_Sale.Plant INNER JOIN tblRevYeo on tblPlantMaster.plantName=tblRevYeo.Plant INNER JOIN tblYEO_A_M on tblPlantMaster.plantName=tblYEO_A_M.Plant INNER JOIN SaleMaster_17_18 ON tblPlantMaster.plantName=SaleMaster_17_18.Plant INNER JOIN Avgmnth_17_18 ON tblPlantMaster.plantName=Avgmnth_17_18.Plant";
            }
            else if (month == "Jan")
            {
                if (MonthList.ContainsKey("Jan") == true)
                {
                    count = MonthList["Jan"].ToString();
                }
                PlantQuery = "SELECT tblPlantMaster.plantName AS Plant, SaleMaster_15_16.sale AS [FY 15-16], SaleMaster_16_17.sale AS [FY 16-17],SaleMaster_17_18.sale As [FY 17-18], Avgmnth_17_18.sale As [Avg/Mnth 17-18], tblBudgetMaster.Budget AS [Bud FY 18-19],CAST(tblBudgetMaster.Budget/12 as numeric(18,2)) as [Monthly Bud], EveryMonth.April, EveryMonth.May, EveryMonth.June, EveryMonth.July, EveryMonth.August,EveryMonth.September,tblRevYeo.YEO as [Rev YEO],tblYEO_A_M.YEO as [YEO Avg/Mnth],EveryMonth.October,EveryMonth.November,EveryMonth.December,EveryMonth.January,cast((CAST(tblRevYeo.YEO as decimal(18,2)) -Cast(April+May+June+July+August+September+October+November+December as numeric(18,2)))/" + Convert.ToInt32(count) + " as numeric(18,2))as [Asking Rate Jan],(April+May+June+July+August+September+October+November+December+January) as [YTD Sale]," + Query1 + " as [Yest Sale (in Lakh)],CAST(Avg_Sale.AvgSale/(select * from DaysCount) as numeric(18,2))AS [Avg/Day(in lac)] FROM         tblPlantMaster INNER JOIN EveryMonth ON tblPlantMaster.plantName = EveryMonth.Plant INNER JOIN SaleMaster_16_17 ON tblPlantMaster.plantName = SaleMaster_16_17.Plant INNER JOIN SaleMaster_15_16 ON tblPlantMaster.plantName = SaleMaster_15_16.Plant INNER JOIN tblBudgetMaster ON tblPlantMaster.plantName = tblBudgetMaster.Plant INNER JOIN Final_Yest ON tblPlantMaster.plantName=Final_Yest.PlantGroup INNER JOIN Avg_Sale ON tblPlantMaster.plantName=Avg_Sale.Plant INNER JOIN tblRevYeo on tblPlantMaster.plantName=tblRevYeo.Plant INNER JOIN tblYEO_A_M on tblPlantMaster.plantName=tblYEO_A_M.Plant INNER JOIN SaleMaster_17_18 ON tblPlantMaster.plantName=SaleMaster_17_18.Plant INNER JOIN Avgmnth_17_18 ON tblPlantMaster.plantName=Avgmnth_17_18.Plant";
            }
            else if (month == "Feb")
            {
                if (MonthList.ContainsKey("Feb") == true)
                {
                    count = MonthList["Feb"].ToString();
                }
                PlantQuery = "SELECT tblPlantMaster.plantName AS Plant, SaleMaster_15_16.sale AS [FY 15-16], SaleMaster_16_17.sale AS [FY 16-17],SaleMaster_17_18.sale As [FY 17-18], Avgmnth_17_18.sale As [Avg/Mnth 17-18], tblBudgetMaster.Budget AS [Bud FY 18-19],CAST(tblBudgetMaster.Budget/12 as numeric(18,2)) as [Monthly Bud], EveryMonth.April, EveryMonth.May, EveryMonth.June, EveryMonth.July, EveryMonth.August,EveryMonth.September,tblRevYeo.YEO as [Rev YEO],tblYEO_A_M.YEO as [YEO Avg/Mnth],EveryMonth.October,EveryMonth.November,EveryMonth.December,EveryMonth.January,EveryMonth.February,cast((CAST(tblRevYeo.YEO as decimal(18,2)) -Cast(April+May+June+July+August+September+October+November+December+January as numeric(18,2)))/" + Convert.ToInt32(count) + " as numeric(18,2))as [Asking Rate Feb],(April+May+June+July+August+September+October+November+December+January+February) as [YTD Sale]," + Query1 + " as [Yest Sale (in Lakh)],CAST(Avg_Sale.AvgSale/(select * from DaysCount) as numeric(18,2))AS [Avg/Day(in lac)] FROM         tblPlantMaster INNER JOIN EveryMonth ON tblPlantMaster.plantName = EveryMonth.Plant INNER JOIN SaleMaster_16_17 ON tblPlantMaster.plantName = SaleMaster_16_17.Plant INNER JOIN SaleMaster_15_16 ON tblPlantMaster.plantName = SaleMaster_15_16.Plant INNER JOIN tblBudgetMaster ON tblPlantMaster.plantName = tblBudgetMaster.Plant INNER JOIN Final_Yest ON tblPlantMaster.plantName=Final_Yest.PlantGroup INNER JOIN Avg_Sale ON tblPlantMaster.plantName=Avg_Sale.Plant INNER JOIN tblRevYeo on tblPlantMaster.plantName=tblRevYeo.Plant INNER JOIN tblYEO_A_M on tblPlantMaster.plantName=tblYEO_A_M.Plant INNER JOIN SaleMaster_17_18 ON tblPlantMaster.plantName=SaleMaster_17_18.Plant INNER JOIN Avgmnth_17_18 ON tblPlantMaster.plantName=Avgmnth_17_18.Plant";
            }
            else if (month == "Mar")
            {
                if (MonthList.ContainsKey("Mar") == true)
                {
                    count = MonthList["Mar"].ToString();
                }
                PlantQuery = "SELECT tblPlantMaster.plantName AS Plant, SaleMaster_15_16.sale AS [FY 15-16], SaleMaster_16_17.sale AS [FY 16-17],SaleMaster_17_18.sale As [FY 17-18], Avgmnth_17_18.sale As [Avg/Mnth 17-18], tblBudgetMaster.Budget AS [Bud FY 18-19],CAST(tblBudgetMaster.Budget/12 as numeric(18,2)) as [Monthly Bud], EveryMonth.April, EveryMonth.May, EveryMonth.June, EveryMonth.July, EveryMonth.August,EveryMonth.September,tblRevYeo.YEO as [Rev YEO],tblYEO_A_M.YEO as [YEO Avg/Mnth],EveryMonth.October,EveryMonth.November,EveryMonth.December,EveryMonth.January,EveryMonth.February,EveryMonth.March,cast((CAST(tblRevYeo.YEO as decimal(18,2)) -Cast(April+May+June+July+August+September+October+November+December+January+February as numeric(18,2)))/" + Convert.ToInt32(count) + " as numeric(18,2))as [Asking Rate Mar],(April+May+June+July+August+September+October+November+December+January+February+March) as [YTD Sale]," + Query1 + " as [Yest Sale (in Lakh)],CAST(Avg_Sale.AvgSale/(select * from DaysCount) as numeric(18,2))AS [Avg/Day(in lac)] FROM         tblPlantMaster INNER JOIN EveryMonth ON tblPlantMaster.plantName = EveryMonth.Plant INNER JOIN SaleMaster_16_17 ON tblPlantMaster.plantName = SaleMaster_16_17.Plant INNER JOIN SaleMaster_15_16 ON tblPlantMaster.plantName = SaleMaster_15_16.Plant INNER JOIN tblBudgetMaster ON tblPlantMaster.plantName = tblBudgetMaster.Plant INNER JOIN Final_Yest ON tblPlantMaster.plantName=Final_Yest.PlantGroup INNER JOIN Avg_Sale ON tblPlantMaster.plantName=Avg_Sale.Plant INNER JOIN tblRevYeo on tblPlantMaster.plantName=tblRevYeo.Plant INNER JOIN tblYEO_A_M on tblPlantMaster.plantName=tblYEO_A_M.Plant INNER JOIN SaleMaster_17_18 ON tblPlantMaster.plantName=SaleMaster_17_18.Plant INNER JOIN Avgmnth_17_18 ON tblPlantMaster.plantName=Avgmnth_17_18.Plant";
            }
            else if (month == "Apr")
            {
                if (MonthList.ContainsKey("Apr") == true)
                {
                    count = MonthList["Apr"].ToString();
                }
                PlantQuery = "SELECT tblPlantMaster.plantName AS Plant, SaleMaster_15_16.sale AS [FY 15-16], SaleMaster_16_17.sale AS [FY 16-17],SaleMaster_17_18.sale As [FY 17-18], Avgmnth_17_18.sale As [Avg/Mnth 17-18], tblBudgetMaster.Budget AS [Bud FY 18-19], CAST(tblBudgetMaster.Budget/12 as numeric(18,2)) as [Monthly Bud],  EveryMonth.April, (April) as [YTD Sale], " + Query1 + " as [Yest Sale (in Lakh)], CAST(Avg_Sale.AvgSale/(select * from DaysCount) as numeric(18,2))AS [Avg/Day(in lac)]  FROM         tblPlantMaster INNER JOIN EveryMonth ON tblPlantMaster.plantName = EveryMonth.Plant INNER JOIN SaleMaster_16_17 ON tblPlantMaster.plantName = SaleMaster_16_17.Plant INNER JOIN SaleMaster_15_16 ON tblPlantMaster.plantName = SaleMaster_15_16.Plant INNER JOIN tblBudgetMaster ON tblPlantMaster.plantName = tblBudgetMaster.Plant INNER JOIN Final_Yest ON tblPlantMaster.plantName=Final_Yest.PlantGroup INNER JOIN Avg_Sale ON tblPlantMaster.plantName=Avg_Sale.Plant INNER JOIN tblRevYeo on tblPlantMaster.plantName=tblRevYeo.Plant INNER JOIN tblYEO_A_M on tblPlantMaster.plantName=tblYEO_A_M.Plant INNER JOIN SaleMaster_17_18 ON tblPlantMaster.plantName=SaleMaster_17_18.Plant INNER JOIN Avgmnth_17_18 ON tblPlantMaster.plantName=Avgmnth_17_18.Plant";
            }
            else if (month == "May")
            {
                if (MonthList.ContainsKey("May") == true)
                {
                    count = MonthList["May"].ToString();
                }
                PlantQuery = "SELECT tblPlantMaster.plantName AS Plant, SaleMaster_15_16.sale AS [FY 15-16], SaleMaster_16_17.sale AS [FY 16-17],SaleMaster_17_18.sale As [FY 17-18], Avgmnth_17_18.sale As [Avg/Mnth 17-18], tblBudgetMaster.Budget AS [Bud FY 18-19],CAST(tblBudgetMaster.Budget/12 as numeric(18,2)) as [Monthly Bud], EveryMonth.April,EveryMonth.May,(April+May) as [YTD Sale]," + Query1 + " as [Yest Sale (in Lakh)],CAST(Avg_Sale.AvgSale/(select * from DaysCount) as numeric(18,2))AS [Avg/Day(in lac)] FROM         tblPlantMaster INNER JOIN EveryMonth ON tblPlantMaster.plantName = EveryMonth.Plant INNER JOIN SaleMaster_16_17 ON tblPlantMaster.plantName = SaleMaster_16_17.Plant INNER JOIN SaleMaster_15_16 ON tblPlantMaster.plantName = SaleMaster_15_16.Plant INNER JOIN tblBudgetMaster ON tblPlantMaster.plantName = tblBudgetMaster.Plant INNER JOIN Final_Yest ON tblPlantMaster.plantName=Final_Yest.PlantGroup INNER JOIN Avg_Sale ON tblPlantMaster.plantName=Avg_Sale.Plant INNER JOIN tblRevYeo on tblPlantMaster.plantName=tblRevYeo.Plant INNER JOIN tblYEO_A_M on tblPlantMaster.plantName=tblYEO_A_M.Plant INNER JOIN SaleMaster_17_18 ON tblPlantMaster.plantName=SaleMaster_17_18.Plant INNER JOIN Avgmnth_17_18 ON tblPlantMaster.plantName=Avgmnth_17_18.Plant";
            }
            else if (month == "Jun")
            {
                if (MonthList.ContainsKey("Jun") == true)
                {
                    count = MonthList["Jun"].ToString();
                }
                PlantQuery = "SELECT tblPlantMaster.plantName AS Plant, SaleMaster_15_16.sale AS [FY 15-16], SaleMaster_16_17.sale AS [FY 16-17],SaleMaster_17_18.sale As [FY 17-18], Avgmnth_17_18.sale As [Avg/Mnth 17-18], tblBudgetMaster.Budget AS [Bud FY 18-19],CAST(tblBudgetMaster.Budget/12 as numeric(18,2)) as [Monthly Bud], EveryMonth.April, EveryMonth.May, EveryMonth.June, (April+May+June) as [YTD Sale]," + Query1 + " as [Yest Sale (in Lakh)],CAST(Avg_Sale.AvgSale/(select * from DaysCount) as numeric(18,2))AS [Avg/Day(in lac)] FROM         tblPlantMaster INNER JOIN EveryMonth ON tblPlantMaster.plantName = EveryMonth.Plant INNER JOIN SaleMaster_16_17 ON tblPlantMaster.plantName = SaleMaster_16_17.Plant INNER JOIN SaleMaster_15_16 ON tblPlantMaster.plantName = SaleMaster_15_16.Plant INNER JOIN tblBudgetMaster ON tblPlantMaster.plantName = tblBudgetMaster.Plant INNER JOIN Final_Yest ON tblPlantMaster.plantName=Final_Yest.PlantGroup INNER JOIN Avg_Sale ON tblPlantMaster.plantName=Avg_Sale.Plant INNER JOIN tblRevYeo on tblPlantMaster.plantName=tblRevYeo.Plant INNER JOIN tblYEO_A_M on tblPlantMaster.plantName=tblYEO_A_M.Plant INNER JOIN SaleMaster_17_18 ON tblPlantMaster.plantName=SaleMaster_17_18.Plant INNER JOIN Avgmnth_17_18 ON tblPlantMaster.plantName=Avgmnth_17_18.Plant";
            }
            else if (month == "Jul")
            {
                if (MonthList.ContainsKey("Jul") == true)
                {
                    count = MonthList["Jul"].ToString();
                }
                PlantQuery = "SELECT tblPlantMaster.plantName AS Plant, SaleMaster_15_16.sale AS [FY 15-16], SaleMaster_16_17.sale AS [FY 16-17],SaleMaster_17_18.sale As [FY 17-18], Avgmnth_17_18.sale As [Avg/Mnth 17-18], tblBudgetMaster.Budget AS [Bud FY 18-19],CAST(tblBudgetMaster.Budget/12 as numeric(18,2)) as [Monthly Bud], EveryMonth.April, EveryMonth.May, EveryMonth.June,EveryMonth.July, (April+May+June+July) as [YTD Sale]," + Query1 + " as [Yest Sale (in Lakh)],CAST(Avg_Sale.AvgSale/(select * from DaysCount) as numeric(18,2))AS [Avg/Day(in lac)] FROM         tblPlantMaster INNER JOIN EveryMonth ON tblPlantMaster.plantName = EveryMonth.Plant INNER JOIN SaleMaster_16_17 ON tblPlantMaster.plantName = SaleMaster_16_17.Plant INNER JOIN SaleMaster_15_16 ON tblPlantMaster.plantName = SaleMaster_15_16.Plant INNER JOIN tblBudgetMaster ON tblPlantMaster.plantName = tblBudgetMaster.Plant INNER JOIN Final_Yest ON tblPlantMaster.plantName=Final_Yest.PlantGroup INNER JOIN Avg_Sale ON tblPlantMaster.plantName=Avg_Sale.Plant INNER JOIN tblRevYeo on tblPlantMaster.plantName=tblRevYeo.Plant INNER JOIN tblYEO_A_M on tblPlantMaster.plantName=tblYEO_A_M.Plant INNER JOIN SaleMaster_17_18 ON tblPlantMaster.plantName=SaleMaster_17_18.Plant INNER JOIN Avgmnth_17_18 ON tblPlantMaster.plantName=Avgmnth_17_18.Plant";
            }
            else if (month == "Aug")
            {
                if (MonthList.ContainsKey("Aug") == true)
                {
                    count = MonthList["Aug"].ToString();
                }
                PlantQuery = "SELECT tblPlantMaster.plantName AS Plant, SaleMaster_15_16.sale AS [FY 15-16], SaleMaster_16_17.sale AS [FY 16-17],SaleMaster_17_18.sale As [FY 17-18], Avgmnth_17_18.sale As [Avg/Mnth 17-18], tblBudgetMaster.Budget AS [Bud FY 18-19],CAST(tblBudgetMaster.Budget/12 as numeric(18,2)) as [Monthly Bud], EveryMonth.April, EveryMonth.May, EveryMonth.June,EveryMonth.July,EveryMonth.August, (April+May+June+July+August) as [YTD Sale]," + Query1 + " as [Yest Sale (in Lakh)],CAST(Avg_Sale.AvgSale/(select * from DaysCount) as numeric(18,2))AS [Avg/Day(in lac)] FROM         tblPlantMaster INNER JOIN EveryMonth ON tblPlantMaster.plantName = EveryMonth.Plant INNER JOIN SaleMaster_16_17 ON tblPlantMaster.plantName = SaleMaster_16_17.Plant INNER JOIN SaleMaster_15_16 ON tblPlantMaster.plantName = SaleMaster_15_16.Plant INNER JOIN tblBudgetMaster ON tblPlantMaster.plantName = tblBudgetMaster.Plant INNER JOIN Final_Yest ON tblPlantMaster.plantName=Final_Yest.PlantGroup INNER JOIN Avg_Sale ON tblPlantMaster.plantName=Avg_Sale.Plant INNER JOIN tblRevYeo on tblPlantMaster.plantName=tblRevYeo.Plant INNER JOIN tblYEO_A_M on tblPlantMaster.plantName=tblYEO_A_M.Plant INNER JOIN SaleMaster_17_18 ON tblPlantMaster.plantName=SaleMaster_17_18.Plant INNER JOIN Avgmnth_17_18 ON tblPlantMaster.plantName=Avgmnth_17_18.Plant";
            }
            else if (month == "Sep")
            {
                if (MonthList.ContainsKey("Sep") == true)
                {
                    count = MonthList["Sep"].ToString();
                }
                PlantQuery = "SELECT tblPlantMaster.plantName AS Plant, SaleMaster_15_16.sale AS [FY 15-16], SaleMaster_16_17.sale AS [FY 16-17],SaleMaster_17_18.sale As [FY 17-18], Avgmnth_17_18.sale As [Avg/Mnth 17-18], tblBudgetMaster.Budget AS [Bud FY 18-19],CAST(tblBudgetMaster.Budget/12 as numeric(18,2)) as [Monthly Bud], EveryMonth.April, EveryMonth.May, EveryMonth.June,EveryMonth.July,EveryMonth.August,EveryMonth.September, tblRevYeo.YEO as [Rev YEO],tblYEO_A_M.YEO as [YEO Avg/Mnth],cast((CAST(tblRevYeo.YEO as decimal(18,2)) -Cast(April+May+June+July+August as numeric(18,2)))/" + Convert.ToInt32(count) + " as numeric(18,2))as [Asking Rate Sep],(April+May+June+July+August+September) as [YTD Sale]," + Query1 + " as [Yest Sale (in Lakh)],CAST(Avg_Sale.AvgSale/(select * from DaysCount) as numeric(18,2))AS [Avg/Day(in lac)] FROM         tblPlantMaster INNER JOIN EveryMonth ON tblPlantMaster.plantName = EveryMonth.Plant INNER JOIN SaleMaster_16_17 ON tblPlantMaster.plantName = SaleMaster_16_17.Plant INNER JOIN SaleMaster_15_16 ON tblPlantMaster.plantName = SaleMaster_15_16.Plant INNER JOIN tblBudgetMaster ON tblPlantMaster.plantName = tblBudgetMaster.Plant INNER JOIN Final_Yest ON tblPlantMaster.plantName=Final_Yest.PlantGroup INNER JOIN Avg_Sale ON tblPlantMaster.plantName=Avg_Sale.Plant INNER JOIN tblRevYeo on tblPlantMaster.plantName=tblRevYeo.Plant INNER JOIN tblYEO_A_M on tblPlantMaster.plantName=tblYEO_A_M.Plant INNER JOIN SaleMaster_17_18 ON tblPlantMaster.plantName=SaleMaster_17_18.Plant INNER JOIN Avgmnth_17_18 ON tblPlantMaster.plantName=Avgmnth_17_18.Plant";
            }
            else if (month == "Oct")
            {
                if (MonthList.ContainsKey("Oct") == true)
                {
                    count = MonthList["Oct"].ToString();
                }
                PlantQuery = "SELECT tblPlantMaster.plantName AS Plant, SaleMaster_15_16.sale AS [FY 15-16], SaleMaster_16_17.sale AS [FY 16-17],SaleMaster_17_18.sale As [FY 17-18], Avgmnth_17_18.sale As [Avg/Mnth 17-18], tblBudgetMaster.Budget AS [Bud FY 18-19],CAST(tblBudgetMaster.Budget/12 as numeric(18,2)) as [Monthly Bud], EveryMonth.April, EveryMonth.May, EveryMonth.June,EveryMonth.July,EveryMonth.August,EveryMonth.September, tblRevYeo.YEO as [Rev YEO],tblYEO_A_M.YEO as [YEO Avg/Mnth],EveryMonth.October,cast((CAST(tblRevYeo.YEO as decimal(18,2)) -Cast(April+May+June+July+August+September+October as numeric(18,2)))/" + Convert.ToInt32(count) + " as numeric(18,2))as [Asking Rate Oct],(April+May+June+July+August+September) as [YTD Sale]," + Query1 + " as [Yest Sale (in Lakh)],CAST(Avg_Sale.AvgSale/(select * from DaysCount) as numeric(18,2))AS [Avg/Day(in lac)] FROM         tblPlantMaster INNER JOIN EveryMonth ON tblPlantMaster.plantName = EveryMonth.Plant INNER JOIN SaleMaster_16_17 ON tblPlantMaster.plantName = SaleMaster_16_17.Plant INNER JOIN SaleMaster_15_16 ON tblPlantMaster.plantName = SaleMaster_15_16.Plant INNER JOIN tblBudgetMaster ON tblPlantMaster.plantName = tblBudgetMaster.Plant INNER JOIN Final_Yest ON tblPlantMaster.plantName=Final_Yest.PlantGroup INNER JOIN Avg_Sale ON tblPlantMaster.plantName=Avg_Sale.Plant INNER JOIN tblRevYeo on tblPlantMaster.plantName=tblRevYeo.Plant INNER JOIN tblYEO_A_M on tblPlantMaster.plantName=tblYEO_A_M.Plant INNER JOIN SaleMaster_17_18 ON tblPlantMaster.plantName=SaleMaster_17_18.Plant INNER JOIN Avgmnth_17_18 ON tblPlantMaster.plantName=Avgmnth_17_18.Plant";
            }
        }

        private void LoadPlantSaleSummaary()
        {
            DataTable dt = new DataTable();
            try
            {
                string query = "";
                string dateNow = "";
                string month = DateTime.Now.ToString("MMM");
                dateNow = DateTime.Now.ToShortDateString();
                ReturnMonth();
                SqlCommand cmd = new SqlCommand(PlantQuery, constr);
                constr.Open();
                SqlDataReader dr = cmd.ExecuteReader(CommandBehavior.CloseConnection);
                DataTable dtSch = dr.GetSchemaTable();
                dt.Load(dr);

                //Adding inversion code
                double sum = 0, val = 0;
                DataTable invertDT = new DataTable();
                invertDT = GetInversedDataTable(dt, "Plant", "");
                invertDT.Columns.Add("Total");

                for (int i = 0; i < invertDT.Rows.Count; i++)
                {
                    for (int j = 0; j < invertDT.Columns.Count; j++)
                    {
                        if (j != 0)
                        {
                            sum = sum + DBNulls.NumberValue(invertDT.Rows[i][j]);
                        }
                        val = j;
                    }
                    invertDT.Rows[i][invertDT.Columns.Count - 1] = sum;
                    sum = 0;
                }

                string result = string.Empty;

                RadGridPlant.DataSource = invertDT;
                RadGridPlant.DataBind();

            }
            catch (Exception ex)
            {
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alertMessage", "alert(" + ex.Message + ")", true);
            }
            finally
            {
                constr.Close();
            }
        }

        private void LoadKPlantSaleSummaary()
        {
            DataTable dt = new DataTable();
            try
            {
                string dateNow = "";
                dateNow = DateTime.Now.ToShortDateString();

                string query = "select tblCustomerMaster.CustomerAcronym As Customer, tblCustomerMaster.CustomerCode as Code, cast(SUM(CAST(Col6 as numeric(18,2)))/100000 as numeric(18,2))  as [Plan],cast(SUM(CAST(Col10 as numeric(18,2)))/100000 as numeric(18,2)) as Actual,CAST(SUM(CAST(dbo.tblSalesDb.Col6 AS numeric(18, 2))) / 100000 AS numeric(18, 2)) - CAST(SUM(CAST(dbo.tblSalesDb.Col10 AS numeric(18, 2))) / 100000 AS numeric(18, 2)) as GAP from tblSalesDb,tblCustomerMaster where Col2=tblCustomerMaster.CustomerCode and Col17='K96' group by tblCustomerMaster.CustomerAcronym,tblCustomerMaster.CustomerCode order by Customer asc";

                SqlCommand cmd = new SqlCommand(query, constr);
                constr.Open();
                SqlDataReader dr = cmd.ExecuteReader(CommandBehavior.CloseConnection);
                DataTable dtSch = dr.GetSchemaTable();
                dt.Load(dr);

                //Adding inversion code
                //DataTable invertDT = new DataTable();
                //invertDT = GetInversedDataTable(dt, "Customer Name", "");
                RadGrid1.DataSource = dt;
                RadGrid1.DataBind();
            }
            catch (Exception ex)
            {
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alertMessage", "alert(" + ex.Message + ")", true);
            }
            finally
            {
                constr.Close();
            }
        }

        private void LoadPPlantSaleSummaary()
        {
            DataTable dt = new DataTable();
            try
            {
                string dateNow = "";
                dateNow = DateTime.Now.ToShortDateString();

                string query = "select tblPCustomerMaster.CustomerAcronym Customer,cast(SUM(CAST(Col6 as numeric(18,2)))/100000 as numeric(18,2))  as [Plan],cast(SUM(CAST(Col10 as numeric(18,2)))/100000 as numeric(18,2)) as Actual,CAST(SUM(CAST(dbo.tblSalesDb.Col6 AS numeric(18, 2))) / 100000 AS numeric(18, 2)) - CAST(SUM(CAST(dbo.tblSalesDb.Col10 AS numeric(18, 2))) / 100000 AS numeric(18, 2)) as GAP from tblSalesDb,tblPCustomerMaster where Col2=tblPCustomerMaster.CustomerCode and Col17='P115' group by tblPCustomerMaster.CustomerAcronym";

                SqlCommand cmd = new SqlCommand(query, constr);
                constr.Open();
                SqlDataReader dr = cmd.ExecuteReader(CommandBehavior.CloseConnection);
                DataTable dtSch = dr.GetSchemaTable();
                dt.Load(dr);

                RadGrid2.DataSource = dt;
                RadGrid2.DataBind();
            }
            catch (Exception ex)
            {
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alertMessage", "alert(" + ex.Message + ")", true);
            }
            finally
            {
                constr.Close();
            }
        }

        private void LoadMatSummary()
        {
            DataTable dt = new DataTable();
            try
            {
                string dateNow = "";
                dateNow = DateTime.Now.ToShortDateString();
                string qry, Tdays = string.Empty;
                qry = "select TotalDays from TotalWorkdays";
                Tdays = DBUtils.SqlSelectScalar(new SqlCommand(qry));
                string query = "SELECT     Col4 AS Part, Col2 AS [Customer Code], Col3 AS [Desc], CAST(CAST(Col5 AS numeric(18, 2)) / '" + Tdays + "' AS numeric(18, 0)) AS [Per Day Reqd], " + Query2 + " AS [YestDay Sale Qty], CAST(Col9 / (SELECT DayCount FROM dbo.DaysCount) AS numeric(18, 0)) AS Average FROM dbo.tblSalesDb WHERE (Col4 IN (SELECT DISTINCT DevPartNo FROM dbo.tblDevParts)) AND (Col14 = 'REG') GROUP BY Col4, Col3, Col5, Col13, Col9, Col16, Col2";
                SqlCommand cmd = new SqlCommand(query, constr);
                constr.Open();
                SqlDataReader dr = cmd.ExecuteReader(CommandBehavior.CloseConnection);
                DataTable dtSch = dr.GetSchemaTable();
                dt.Load(dr);
                RadGrid4.DataSource = dt;
                RadGrid4.DataBind();
            }
            catch (Exception ex)
            {
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alertMessage", "alert(" + ex.Message + ")", true);
            }
            finally
            {
                constr.Close();
            }
        }

        private void LoadBajajSpares()
        {
            DataTable dt = new DataTable();
            try
            {
                string dateNow = "";
                dateNow = DateTime.Now.ToShortDateString();

                string query = "select [Plan],Actual,Percentage from BajajSpares";
                SqlCommand cmd = new SqlCommand(query, constr);
                constr.Open();
                SqlDataReader dr = cmd.ExecuteReader(CommandBehavior.CloseConnection);
                DataTable dtSch = dr.GetSchemaTable();
                dt.Load(dr);
                RadGrid3.DataSource = dt;
                RadGrid3.DataBind();
            }
            catch (Exception ex)
            {
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alertMessage", "alert(" + ex.Message + ")", true);
            }
            finally
            {
                constr.Close();
            }
        }

        private void ReadSyncTime()
        {
            try
            {
                constr.Open();
                String Qry = "Select PrimaryColumn from tblSalesDb";
                String Qry2 = "Select * from DaysCount";
                string Qry3 = "select * from TotalWorkdays";
                SqlCommand cmd = new SqlCommand(Qry, constr);
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                SqlDataReader dr = cmd.ExecuteReader();
                dr.Read();
                Label1.Text = "Last updated on: " + dr["PrimaryColumn"].ToString();

                SqlCommand cmd2 = new SqlCommand(Qry2, constr);
                SqlDataAdapter da2 = new SqlDataAdapter(cmd2);
                SqlDataReader dr2 = cmd2.ExecuteReader();
                dr2.Read();

                SqlCommand cmd3 = new SqlCommand(Qry3, constr);
                SqlDataAdapter da3 = new SqlDataAdapter(cmd3);
                SqlDataReader dr3 = cmd3.ExecuteReader();
                dr3.Read();

                string TotalworkingDays = dr3["TotalDays"].ToString();
                rdWorkingDays.Text = "Working Days:-" + dr2["DayCount"].ToString() + "/ " + dr3["TotalDays"].ToString() + "";
            }
            catch (Exception ex)
            {
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alertMessage", "alert(" + ex.Message + ")", true);
            }
            finally
            {
                constr.Close();
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                LoadIfHoliday();
                LoadPlantSaleSummaary();
                LoadKPlantSaleSummaary();
                LoadPPlantSaleSummaary();
                LoadMatSummary();
                LoadBajajSpares();
                ReadSyncTime();
                //Literal_Timeline();

            }

            string value = string.Empty;
        }

        protected void RadButton1_Click(object sender, EventArgs e)
        {
            //Response.Redirect("");
        }

        protected void RadGrid4_ItemDataBound(object sender, GridItemEventArgs e)
        {
            if (e.Item is GridDataItem)
            {
                GridDataItem dataItem = (GridDataItem)e.Item;
                TableCell PerDay = dataItem["PerDayReqd"];
                TableCell Avg = dataItem["Average"];
                if (DBNulls.NumberValue(PerDay.Text) > DBNulls.NumberValue(Avg.Text))
                {
                    //dataItem.BackColor = System.Drawing.Color.Red;  
                    dataItem["Average"].ForeColor = System.Drawing.Color.Red;
                    //dataItem.Font.Bold = true;
                }
            }
        }

        protected void RadGridPlant_ItemDataBound(object sender, Telerik.Web.UI.GridItemEventArgs e)
        {
            try
            {
                if (e.Item is GridDataItem)
                {

                    GridDataItem item = (GridDataItem)e.Item;
                    string value = item.Cells[3].ToString();
                }

                //Is it a GridDataItem
                if (e.Item is GridDataItem)
                {
                    //Get the instance of the right type
                    GridDataItem dataBoundItem = e.Item as GridDataItem;
                    //if(dataBoundItem.GetDataKeyValue("ID").ToString() == "you Compared Text") // you can also use datakey also
                    if (dataBoundItem["Plant"].Text == "Rev YEO")
                    {
                        //dataBoundItem["Plant"].ForeColor = System.Drawing.Color.Red; // chanmge particuler cell
                        e.Item.BackColor = System.Drawing.Color.Orange; // for whole row
                        e.Item.Font.Bold = true;
                        //dataItem.CssClass = "MyMexicoRowClass";
                    }
                    else if (dataBoundItem["Plant"].Text == "YEO Avg/Mnth")
                    {
                        //dataBoundItem["Plant"].ForeColor = System.Drawing.Color.Green; // chanmge particuler cell
                        e.Item.BackColor = System.Drawing.Color.Orange; // for whole row
                        e.Item.Font.Bold = true;
                        //dataItem.CssClass = "MyMexicoRowClass";
                    }
                    else if (dataBoundItem["Plant"].Text == "Asking Rate "+month)
                    {
                        //dataBoundItem["Plant"].ForeColor = System.Drawing.Color.Green; // chanmge particuler cell
                        e.Item.BackColor = System.Drawing.Color.Orange; // for whole row
                        e.Item.Font.Bold = true;
                        //dataItem.CssClass = "MyMexicoRowClass";
                    }
                    else if (dataBoundItem["Plant"].Text == "Bud FY 18-19")
                    {
                        e.Item.BackColor = System.Drawing.Color.LightGreen; // for whole row
                    }
                    else if (dataBoundItem["Plant"].Text == "Monthly Bud")
                    {
                        e.Item.BackColor = System.Drawing.Color.LightGreen; // for whole row
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        protected void RadGrid1_PreRender(object sender, EventArgs e)
        {
            for (int rowIndex = RadGrid1.Items.Count - 2; rowIndex >= 0; rowIndex--)
            {
                GridDataItem row = RadGrid1.Items[rowIndex];
                GridDataItem previousRow = RadGrid1.Items[rowIndex + 1];

                for (int i = 0; i < row.Cells.Count; i++)
                {
                    if (row.Cells[i].Text == previousRow.Cells[i].Text)
                    {
                        row.Cells[i].RowSpan = previousRow.Cells[i].RowSpan < 2 ? 2 : previousRow.Cells[i].RowSpan + 1;
                        previousRow.Cells[i].Visible = false;
                    }
                }
            }
        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            try
            {

                if (txtUsername.Text.ToUpper() == WebConfigurationManager.AppSettings["Admin"].ToString().ToUpper())
                {
                    if (txtPassword.Text == WebConfigurationManager.AppSettings["Password"].ToString())
                    {
                        DateTime Today_date = DateTime.Now;
                        string Today_date_rtc = PublicMethods.fnGetUsableRTC_RTC(Today_date);

                        ////string qry = "insert into tbl_UserLogDetails(User_EmailID,User_Name,Role,Datetime,Datetime_RTC) values ('" + txtUsername.Text + "','Admin','Admin','" + Today_date + "','" + Today_date_rtc + "') ";
                        ////cmd = new SqlCommand(qry);
                        ////DBUtils.ExecuteSQLCommand(cmd);

                        Session.Add("LoginUserEmail", txtUsername.Text);
                        Session.Add("Role", WebConfigurationManager.AppSettings["Admin"].ToString().ToUpper());
                        Session.Add("Password", txtPassword.Text);
                        Response.Redirect("~/sales/frmHolidayMaster.aspx", false);
                        HttpContext.Current.ApplicationInstance.CompleteRequest();


                    }
                    else
                    {
                        ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alertMessage", "alert('Password is wrong')", true);
                        return;
                    }

                }


                else
                {

                    if (txtUsername.Text.Contains("@vtelebyte.com"))
                    {

                        DateTime Today_date = DateTime.Now;
                        string Today_date_rtc = PublicMethods.fnGetUsableRTC_RTC(Today_date);

                        ////string qry = "insert into tbl_UserLogDetails(User_EmailID,User_Name,Role,Datetime,Datetime_RTC) values ('" + txtUsername.Text + "','Admin','" + cmbRolecode.SelectedValue + "','" + Today_date + "','" + Today_date_rtc + "') ";
                        ////cmd = new SqlCommand(qry);
                        ////DBUtils.ExecuteSQLCommand(cmd);

                        Session.Add("LoginUserEmail", txtUsername.Text);
                        Session.Add("Password", txtPassword.Text);
                        Response.Redirect("~/sales/frmHolidayMaster.aspx", false);
                        HttpContext.Current.ApplicationInstance.CompleteRequest();
                    }
                    else
                    {

                        if (txtUsername.Text.Contains("@sanjeevgroup.com"))
                        {

                        }
                        else
                        {
                            txtUsername.Text = txtUsername.Text + "@sanjeevgroup.com";
                        }

                        bool result = false;
                        bool result1 = false;
                        HttpResponse<string> response = null;
                        try
                        {
                            response = Unirest.get("https://outlook.office365.com/api/v1.0/me/").basicAuth(txtUsername.Text, txtPassword.Text).asJson<string>();
                        }
                        catch (Exception ex)
                        {
                            lblMessage.Text = "Login Failed";
                            return;
                        }
                        if (response.Code == 200)
                            result = true;

                        if (result)
                        {
                            string Name = response.Body.ToString();
                            string[] lines = Name.Split(new[] { ':' }, StringSplitOptions.RemoveEmptyEntries);

                            string line = lines[6].ToString();
                            string[] pair = line.Split(new[] { ',' });
                            string Login_Name = pair[0].Trim().ToString();

                            string qry;
                            System.Data.DataTable dt;
                            DateTime Today_date = DateTime.Now;
                            string Today_date_rtc = PublicMethods.fnGetUsableRTC_RTC(Today_date);

                            DataTable dtrole = new DataTable();


                            //////qry = "insert into tbl_UserLogDetails(User_EmailID,User_Name,Role,Datetime,Datetime_RTC) values ('" + txtUsername.Text + "','" + Login_Name + "','" + cmbRolecode.SelectedValue + "','" + Today_date + "','" + Today_date_rtc + "') ";
                            //////cmd = new SqlCommand(qry);
                            //////DBUtils.ExecuteSQLCommand(cmd);


                            Session.Add("LoginUserEmail", txtUsername.Text);
                            Session.Add("Password", txtPassword.Text);
                            Response.Redirect("~/sales/frmHolidayMaster.aspx", false);
                            HttpContext.Current.ApplicationInstance.CompleteRequest();

                        }
                        else
                        {
                            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alertMessage", "alert('Email/Password is wrong')", true);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void fnInsertUserLog(string logUserName, string dateRTC)
        {

        }

        protected void Literal_Timeline()
        {
            Literal1.Text = "<ul class='timeline-events'> <li class='timeline-event-years-3 timeline-event-legend'></li><li class='timeline-event-years-3'><h2>BEFORE-2015</h2><h3>Initial</h3><h4>Below 25 cr</h4></li><li class='timeline-event-years-2'><h2>2015 Sep</h2><h3>Crossed 25 cr</h3><h4 style='font-weight:bold; font-size:14px;'>26.49 cr</h4></li><li class='timeline-event-years-1'><h2 style='font-weight:bold;'>2017-Present</h2><h3>New Achievement</h3><h4 style='font-weight:bold; font-size:18px;'>31.26 cr</h4></li></ul><ul class='timelines-years'><li>2011</li><li>2013</li><li>2015</li><li>2017</li><li>2019</li></ul><script src='http://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js'></script>";
        }

        protected void ddlTest_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlTest.SelectedValue == "1")
            {

                Response.Redirect("frmLoginNew.aspx?type=calendar");
            }
            else if (ddlTest.SelectedValue == "2")
            {

                Response.Redirect("frmLoginNew.aspx?type=REV");
            }
            else if (ddlTest.SelectedValue == "3")
            {
                Response.Redirect("frmLoginNew.aspx?type=YEO");
            }
        }

    }
}