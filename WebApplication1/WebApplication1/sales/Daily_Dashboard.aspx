﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Daily_Dashboard.aspx.cs" Inherits="WebApplication1.sales.Daily_Dashboard" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<!-- Meta Tag -->
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<!-- SEO -->
<meta name="description" content="150 words">
<meta name="author" content="uipasta">
<meta name="url" content="http://www.yourdomainname.com">
<meta name="copyright" content="company name">
<meta name="robots" content="index,follow">
<meta http-equiv="refresh" content="900">
<head id="Head1" runat="server">
    <title>Daily Dashboard</title>
    <script type="text/javascript">
        function BatchEditCellValueChanged(sender, args) {

            var grid = sender;

            var row = args.get_row();
            var cell = args.get_cell();
            var tableView = args.get_tableView();
            var column = args.get_column();
            var columnUniqueName = args.get_columnUniqueName();
            var editorValue = args.get_editorValue();
            var cellValue = args.get_cellValue();
            alert(editorValue);

            var columnlength = grid.get_masterTableView().get_columns().length;
            for (i = 0 ; i < columnlength; i++) {
                cell = grid.get_masterTableView().get_columns()[i].get_uniqueName();
                alert(cell);
            }
        }
    </script>

    <!-- Favicon -->
    <link rel="shortcut icon" href="images/favicon/favicon.ico">
    <!-- All CSS Plugins -->
    <link rel="stylesheet" type="text/css" href="css/plugin.css">
    <!-- Main CSS Stylesheet -->
    <link rel="stylesheet" type="text/css" href="css/style.css">
    <!-- Timeline CSS Stylesheet -->
    <link rel="stylesheet" type="text/css" href="css/styleTimeline.css">
    <!-- Google Web Fonts  -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Poppins:400,300,500,600,700">
    <style type="text/css">
        .navbar-header {
            text-align: center;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server" style="background-color: white;">
        <asp:ScriptManager ID="ScriptManager1" runat="server">
            <Scripts>
                <asp:ScriptReference Name="jquery" />
                <asp:ScriptReference Name="jquery.ui.combined" />
            </Scripts>
        </asp:ScriptManager>
        <!--Menu section Start-->
        <div style="width: 100%;background:white; padding-bottom: 80px;">
            <table class="col-lg-11">
                <tr>
                    <td style="width: 14%">
                       <%-- <img src="../Images/sanjeev.png" />--%>
                         <img src="../Images/logo.png" />
                    </td>
                    <td style="color: green;margin-left:370px">
                        <h2>Daily Sales Monitoring
                        </h2>
                    </td>
                    <td>
                         <asp:dropdownlist Width="140%" Font-Size="20px" runat="server" id="ddlTest" OnSelectedIndexChanged="ddlTest_SelectedIndexChanged" AutoPostBack="true" > 
                             <asp:listitem text="Select Master" value="0"></asp:listitem>
                             <asp:listitem text="Premium Freight" value="1"></asp:listitem>
                             <asp:listitem text="Customer Complaint" value="2"></asp:listitem>
                             <asp:listitem text="Head Count" value="3"></asp:listitem>
                             <asp:listitem text="Purchase Ratio Target" value="4"></asp:listitem>
                             <asp:listitem text="Pitch Idea" value="5"></asp:listitem>
</asp:dropdownlist>
                       <%-- <ul class="nav navbar-nav navbar-right">
                            <li> <a  href="frmLoginNew.aspx?type=pitch">Pitch Idea</a>
                             
                             
                                 
                            </li>
                        </ul>--%>
                      <%--  <ul class="nav navbar-nav navbar-right">
                            <li>
                               
                                 <a href="frmLoginNew.aspx?type=purchase">Purchase Ratio Target</a>
                                
                            </li>
                        </ul>
                         <ul class="nav navbar-nav navbar-right">
                            <li>
                             
                                <a href="frmLoginNew.aspx?type=customer">Customer Complaint</a>
                            </li>
                        </ul>
                         <ul class="nav navbar-nav navbar-right">
                            <li>
                            
                                <a href="frmLoginNew.aspx?type=head">Head Count</a>
                            </li>
                        </ul>
                        <ul class="nav navbar-nav navbar-right">
                            <li>
                            
                                <a href="frmLoginNew.aspx?type=premium">Premium Freight</a>

                                
                            </li>
                        </ul>
                     --%>

                    </td>
                </tr>
            </table>
        </div>
        <!--Menu section End-->

        <!-- About Start -->

        <section id="about" class="about section-space-padding">
            <div class="container">
                <div class="row">
                </div>
            </div>
            <div class="modal fade subscribe padding-top-120" id="subscribemodal" role="dialog">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-body">
                            <div class="row">
                                <div class="col-sm-offset-2 col-xs-offset-0 col-md-8 col-sm-8">
                                    <div class="margin-bottom-50">
                                        <%--<form id="mc-form" method="post" action="http://uipasta.us14.list-manage.com/subscribe/post?u=854825d502cdc101233c08a21&amp;id=86e84d44b7">	
						  <div class="subscribe-form">
							 <input id="mc-email" type="email" placeholder="Email Address" class="text-input">
							  <button class="submit-btn" type="submit">Submit</button>
								</div>
								<label for="mc-email" class="mc-label"></label>
							  </form>--%>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- About End-->
            <div class="container">
                <div class="row">
                    <div class="col-lg-10">
                        <h5><b>DAILY DASH BOARD</b></h5>
                        <asp:GridView ID="GridView1" runat="server" Style="text-align: center; width: 1300px; background-color: white" OnPreRender="GridView1_PreRender" HeaderStyle-BackColor="#0099ff" HeaderStyle-Font-Bold="true" HeaderStyle-Font-Size="Large" HorizontalAlign="Center" AutoGenerateColumns="False" OnRowDataBound="GridView1_RowDataBound">
                            <HeaderStyle BackColor="#0099FF" Font-Bold="True" Font-Size="Large" HorizontalAlign="Center"></HeaderStyle>
                            <Columns>
                                <asp:BoundField HeaderText="Content" DataField="Content">
                                    <ItemStyle Font-Size="Large" />
                                </asp:BoundField>
                                <asp:BoundField HeaderText="UOM" DataField="UOM">
                                    <ItemStyle Font-Size="Large" />
                                    <HeaderStyle HorizontalAlign="Center" />
                                </asp:BoundField>
                                <asp:BoundField HeaderText="Department" DataField="Department" HeaderStyle-Width="50px">
                                    <HeaderStyle Width="50px"></HeaderStyle>
                                    <ItemStyle Font-Size="Large" />
                                </asp:BoundField>
                                <asp:BoundField HeaderText="Plant" DataField="Plant">
                                    <ItemStyle Font-Size="Large" />
                                </asp:BoundField>
                                <asp:BoundField HeaderText="GRP" DataField="GRP">
                                    <ItemStyle Font-Size="Large" />
                                </asp:BoundField>
                                <asp:BoundField HeaderText="C7" DataField="X1">
                                    <ItemStyle Font-Size="Large" />
                                </asp:BoundField>
                                <asp:BoundField HeaderText="C4" DataField="C4">
                                    <ItemStyle Font-Size="Large" />
                                </asp:BoundField>
                                <asp:BoundField HeaderText="K96" DataField="K96">
                                    <ItemStyle Font-Size="Large" />
                                </asp:BoundField>
                                <asp:BoundField HeaderText="E89" DataField="PNST">
                                    <ItemStyle Font-Size="Large" />
                                </asp:BoundField>
                                <asp:BoundField HeaderText="P115" DataField="P115">
                                    <ItemStyle Font-Size="Large" />
                                </asp:BoundField>
                            </Columns>
                        </asp:GridView>
                    </div>
                </div>
            </div>
        </section>

        <div class="container">
            <div class="row">
                <div class="col-lg-10">
                    <asp:GridView ID="GridView2" runat="server" Style="text-align: center; width: 1300px; background-color: white" OnPreRender="GridView1_PreRender" HeaderStyle-BackColor="#0099ff" HeaderStyle-Font-Bold="true" HeaderStyle-Font-Size="Large" HorizontalAlign="Center" AutoGenerateColumns="False">
                        <HeaderStyle BackColor="#0099FF" Font-Bold="True" Font-Size="Large" HorizontalAlign="Center"></HeaderStyle>
                        <Columns>
                            <asp:BoundField HeaderText="Material Type" DataField="Mat Type">
                                <ItemStyle Font-Size="Large" />
                            </asp:BoundField>

                            <asp:BoundField HeaderText="RM" DataField="RM">
                                <ItemStyle Font-Size="Large" />
                                <HeaderStyle HorizontalAlign="Center" />
                            </asp:BoundField>

                            <asp:BoundField HeaderText="CN" DataField="CN">
                                <ItemStyle Font-Size="Large" />
                                <HeaderStyle HorizontalAlign="Center" />
                            </asp:BoundField>

                            <asp:BoundField HeaderText="TC" DataField="TC">
                                <ItemStyle Font-Size="Large" />
                                <HeaderStyle HorizontalAlign="Center" />
                            </asp:BoundField>

                            <asp:BoundField HeaderText="FG" DataField="FG">
                                <ItemStyle Font-Size="Large" />
                                <HeaderStyle HorizontalAlign="Center" />
                            </asp:BoundField>

                            <asp:BoundField HeaderText="TH" DataField="TH">
                                <ItemStyle Font-Size="Large" />
                                <HeaderStyle HorizontalAlign="Center" />
                            </asp:BoundField>

                            <asp:BoundField HeaderText="SF" DataField="SF">
                                <ItemStyle Font-Size="Large" />
                                <HeaderStyle HorizontalAlign="Center" />
                            </asp:BoundField>

                            <asp:BoundField HeaderText="MS" DataField="MS">
                                <ItemStyle Font-Size="Large" />
                                <HeaderStyle HorizontalAlign="Center" />
                            </asp:BoundField>

                            <asp:BoundField HeaderText="JF" DataField="JF">
                                <ItemStyle Font-Size="Large" />
                                <HeaderStyle HorizontalAlign="Center" />
                            </asp:BoundField>

                            <asp:BoundField HeaderText="DV" DataField="DV">
                                <ItemStyle Font-Size="Large" />
                                <HeaderStyle HorizontalAlign="Center" />
                            </asp:BoundField>

                            <asp:BoundField HeaderText="MA" DataField="MA">
                                <ItemStyle Font-Size="Large" />
                                <HeaderStyle HorizontalAlign="Center" />
                            </asp:BoundField>

                            <asp:BoundField HeaderText="LC" DataField="LC">
                                <ItemStyle Font-Size="Large" />
                                <HeaderStyle HorizontalAlign="Center" />
                            </asp:BoundField>
                        </Columns>
                    </asp:GridView>
                </div>
            </div>


        </div>

       
         
        <!-- Footer Start -->
           <div style="position:relative; height:auto;">
            <asp:Literal ID="Literal1" runat="server"></asp:Literal>
        </div>
                                  <div style="background:#0f0f0f;">
         <hr />
        <center> <asp:Label ID="Label1" ForeColor="white" Font-Size="30px" runat="server" Text="Label"></asp:Label></center></div> 
        <!-- Footer End -->
        <!-- Back to Top Start -->
        <a href="#" class="scroll-to-top"><i class="icon-arrow-up-circle"></i></a>
        <!-- Back to Top End -->
        <!-- All Javascript Plugins  -->
        <script type="text/javascript" src="js/jquery.min.js"></script>
        <script type="text/javascript" src="js/plugin.js"></script>
        <!-- Main Javascript File  -->
        <script type="text/javascript" src="js/scripts.js"></script>
    </form>
</body>
</html>
