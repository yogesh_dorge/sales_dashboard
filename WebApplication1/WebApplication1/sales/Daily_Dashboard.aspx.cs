﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using Telerik.Web.UI;
using Telerik;

namespace WebApplication1.sales
{
    public partial class Daily_Dashboard : System.Web.UI.Page
    {
        SqlConnection constr = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnectionString"].ToString());
        SqlConnection constrAudit = new SqlConnection(ConfigurationManager.ConnectionStrings["AuditConnectionString"].ToString());

        public static DataTable GetInversedDataTable(DataTable table, string columnX, params string[] columnsToIgnore)
        {
            //Create a DataTable to Return
            DataTable returnTable = new DataTable();

            if (columnX == "")
                columnX = table.Columns[0].ColumnName;

            //Add a Column at the beginning of the table

            returnTable.Columns.Add(columnX);

            //Read all DISTINCT values from columnX Column in the provided DataTale
            List<string> columnXValues = new List<string>();

            //Creates list of columns to ignore
            List<string> listColumnsToIgnore = new List<string>();
            if (columnsToIgnore.Length > 0)
                listColumnsToIgnore.AddRange(columnsToIgnore);

            if (!listColumnsToIgnore.Contains(columnX))
                listColumnsToIgnore.Add(columnX);

            foreach (DataRow dr in table.Rows)
            {
                string columnXTemp = dr[columnX].ToString();
                //Verify if the value was already listed
                if (!columnXValues.Contains(columnXTemp))
                {
                    //if the value id different from others provided, add to the list of 
                    //values and creates a new Column with its value.
                    columnXValues.Add(columnXTemp);
                    returnTable.Columns.Add(columnXTemp);
                }
                else
                {
                    //Throw exception for a repeated value
                    throw new Exception("The inversion used must have " +
                                        "unique values for column " + columnX);
                }
            }

            //Add a line for each column of the DataTable

            foreach (DataColumn dc in table.Columns)
            {
                if (!columnXValues.Contains(dc.ColumnName) &&
                    !listColumnsToIgnore.Contains(dc.ColumnName))
                {
                    DataRow dr = returnTable.NewRow();
                    dr[0] = dc.ColumnName;
                    returnTable.Rows.Add(dr);
                }
            }

            //Complete the datatable with the values
            for (int i = 0; i < returnTable.Rows.Count; i++)
            {
                for (int j = 1; j < returnTable.Columns.Count; j++)
                {
                    returnTable.Rows[i][j] =
                      table.Rows[j - 1][returnTable.Rows[i][0].ToString()].ToString();
                }
            }
            return returnTable;
        }

        public static void MergeRows(GridView gridView)
        {
            for (int rowIndex = gridView.Rows.Count - 3; rowIndex >= 0; rowIndex--)
            {
                GridViewRow row = gridView.Rows[rowIndex];
                GridViewRow previousRow = gridView.Rows[rowIndex + 1];
                gridView.Rows[rowIndex].BackColor = System.Drawing.Color.White;
                for (int i = 0; i < row.Cells.Count; i++)
                {
                    if (row.Cells[i].Text == previousRow.Cells[i].Text)
                    {
                        row.Cells[i].RowSpan = previousRow.Cells[i].RowSpan < 2 ? 2 :
                                               previousRow.Cells[i].RowSpan + 1;
                        previousRow.Cells[i].Visible = false;
                    }
                }
            }
        }

        public static void MergeColumns(EventArgs e)
        {
            
        }

        private void LoadDDSales()
        {
            DataTable dtContent = new DataTable();
            DataTable dtValues = new DataTable();
            DataTable dtPurchaseRatio = new DataTable();
            DataTable dtNOI = new DataTable();
            DataTable dtNCActual = new DataTable();
            DataTable dtNCTarget = new DataTable();
            DataTable dtCustComp = new DataTable();
            DataTable dtHeadCount = new DataTable();
            DataTable dtPreFrgt = new DataTable();
            DataTable dtPitIde = new DataTable();

            try
            {
                string dateNow = "";
                dateNow = DateTime.Now.ToShortDateString();

                string query = "select Contents,UOM,Dept from tblDDContentMaster where Contents='Sales'";
                string query2 = "select Col1 as Plant,cast(cast(Col4 as numeric(18,2))/10000000 as numeric(18,2)) as Actual,cast(cast(Col6 as numeric(18,2))/10000000 as numeric(18,2)) as PlanTD,cast(cast(Col5 as numeric(18,2))/10000000 as numeric(18,2)) as MPlan from tblMonthSaleSummary where Col3=(select MONTH(GETDATE())) and Col1 in('X1','K96','C4','PNST','P115') and Col2=(select YEAR(GETDATE()))";
                string query3 = "select Col1 as Plant,Col7 as Actual,tblDDTarget_PurchaseRatio.Target as Target from tblDDPurchaseRatio,tblDDTarget_PurchaseRatio where Col1 in('GRP','X1','K96','C4','PNST','P115') and tblDDPurchaseRatio.Col1=tblDDTarget_PurchaseRatio.Plant";
                string query4 = "select Col1 as Plant,cast(Cast(Col8 as numeric(18,2))/100000 as numeric(18,2)) as DIL from tblDDPurchaseRatio where Col1 in('GRP','X1','K96','C4','PNST','P115')";
                string query5 = "select tbl_AuditPlan_Details.Plant,COUNT(*) as 'Close' from tblCARTransaction,tbl_AuditPlan_Details where tblCARTransaction.AuditNo=tbl_AuditPlan_Details.Audit_AuditNo and MRApprovalDate<>'' group by tbl_AuditPlan_Details.Plant";
                string query6 = "select tbl_AuditPlan_Details.Plant, COUNT(*) as 'Open' from tblCARTransaction,tbl_AuditPlan_Details where tblCARTransaction.AuditNo=tbl_AuditPlan_Details.Audit_AuditNo group by tbl_AuditPlan_Details.Plant";
                //Contenet
                //SqlCommand cmd = new SqlCommand(query, constr);
                //constr.Open();
                //SqlDataReader dr = cmd.ExecuteReader(CommandBehavior.CloseConnection);
                //DataTable dtSch = dr.GetSchemaTable();
                //dtContent.Load(dr);

                //Sales
                SqlCommand cmd2 = new SqlCommand(query2, constr);
                constr.Open();
                SqlDataReader dr2 = cmd2.ExecuteReader(CommandBehavior.CloseConnection);
                DataTable dtSch2 = dr2.GetSchemaTable();
                dtValues.Load(dr2);

                //Purchase Ratio
                SqlCommand cmd3 = new SqlCommand(query3, constr);
                constr.Open();
                SqlDataReader dr3 = cmd3.ExecuteReader(CommandBehavior.CloseConnection);
                DataTable dtSch3 = dr3.GetSchemaTable();
                dtPurchaseRatio.Load(dr3);
                DataRow rows = dtPurchaseRatio.NewRow();
                DataRow rowss = dtPurchaseRatio.NewRow();
                rows[0] = "C4";
                rows[1] = dtPurchaseRatio.Rows[1]["Actual"].ToString();
                rows[2] = dtPurchaseRatio.Rows[1]["Target"].ToString();
                rowss[0] = "PNST";
                rowss[1] = dtPurchaseRatio.Rows[3]["Actual"].ToString();
                rowss[2] = dtPurchaseRatio.Rows[3]["Target"].ToString();
                dtPurchaseRatio.Rows.Add(rows);
                dtPurchaseRatio.Rows.Add(rowss);

                //No of items >des. inv. level
                SqlCommand cmd4 = new SqlCommand(query4, constr);
                constr.Open();
                SqlDataReader dr4 = cmd4.ExecuteReader(CommandBehavior.CloseConnection);
                DataTable dtSch4 = dr4.GetSchemaTable();
                dtNOI.Load(dr4);
                DataRow dRow = dtNOI.NewRow();
                DataRow ddRow = dtNOI.NewRow();
                dRow[0] = "C4";
                dRow[1] = dtNOI.Rows[1]["DIL"].ToString();
                ddRow[0] = "PNST";
                ddRow[1] = dtNOI.Rows[2]["DIL"].ToString();
                dtNOI.Rows.Add(dRow);
                dtNOI.Rows.Add(ddRow);

                //Customer Complaints
                SqlCommand cmdCC = new SqlCommand("select Plant,Actual,Target from tblDDCustomerComplaint", constr);
                constr.Open();
                SqlDataReader drCC = cmdCC.ExecuteReader(CommandBehavior.CloseConnection);
                DataTable dtSchCC = drCC.GetSchemaTable();
                dtCustComp.Load(drCC);

                //Audit NC Actual Count
                SqlCommand cmd5 = new SqlCommand(query5, constrAudit);
                constrAudit.Open();
                SqlDataReader dr5 = cmd5.ExecuteReader(CommandBehavior.CloseConnection);
                DataTable dtsch5 = dr5.GetSchemaTable();
                dtNCActual.Load(dr5);

                //Audit NC Targe Count
                SqlCommand cmd6 = new SqlCommand(query6, constrAudit);
                constrAudit.Open();
                SqlDataReader dr6 = cmd6.ExecuteReader(CommandBehavior.CloseConnection);
                DataTable dtsch6 = dr6.GetSchemaTable();
                dtNCTarget.Load(dr6);

                //Premium Freight
                SqlCommand cmdPF = new SqlCommand("select Plant,Air,Domestic from tblddPRemiumFreight", constr);
                constr.Open();
                SqlDataReader drPF = cmdPF.ExecuteReader(CommandBehavior.CloseConnection);
                DataTable dtSchPF = drPF.GetSchemaTable();
                dtPreFrgt.Load(drPF);

                //Head Count
                SqlCommand cmdHC = new SqlCommand("select Plant,Actual,Available,Target from tblDDHeadCount", constr);
                constr.Open();
                SqlDataReader drHC = cmdHC.ExecuteReader(CommandBehavior.CloseConnection);
                DataTable dtSchHC = drHC.GetSchemaTable();
                dtHeadCount.Load(drHC);

                //Pitch Idea
                SqlCommand cmdPI = new SqlCommand("select Plant,Actual,Target from tblDDPitchIdea", constr);
                constr.Open();
                SqlDataReader drPI = cmdPI.ExecuteReader(CommandBehavior.CloseConnection);
                DataTable dtSchPI = drPI.GetSchemaTable();
                dtPitIde.Load(drPI);

                //Adding inversion code for dtValues
                double sum = 0, val = 0;
                DataTable dtInvert = new DataTable();
                dtInvert = GetInversedDataTable(dtValues, "Plant", "");
                dtInvert.Columns.Add("GRP");
                for (int i = 0; i < dtInvert.Rows.Count; i++)
                {
                    for (int j = 0; j < dtInvert.Columns.Count; j++)
                    {
                        if (j != 0)
                        {
                            sum = sum + DBNulls.NumberValue(dtInvert.Rows[i][j]);
                        }
                        val = j;
                    }
                    dtInvert.Rows[i][dtInvert.Columns.Count - 1] = sum;
                    sum = 0;
                }
                dtInvert.Columns.Add("Content", typeof(System.String)).SetOrdinal(0);
                dtInvert.Columns.Add("UOM", typeof(System.String)).SetOrdinal(1);
                dtInvert.Columns.Add("Department", typeof(System.String)).SetOrdinal(2);
                foreach (DataRow row in dtInvert.Rows)
                {
                    //need to set value to NewColumn column
                    row["Content"] = "Sales";   // or set it to some other value
                    row["UOM"] = "CR";
                    row["Department"] = "Marketing";
                }

                //Adding inversion code for dtPurchaseRatio
                DataTable dtInvert2 = new DataTable();
                dtInvert2 = GetInversedDataTable(dtPurchaseRatio, "Plant", "");
                dtInvert2.Columns.Add("Content", typeof(System.String)).SetOrdinal(0);
                dtInvert2.Columns.Add("UOM", typeof(System.String)).SetOrdinal(1);
                dtInvert2.Columns.Add("Department", typeof(System.String)).SetOrdinal(2);
                foreach (DataRow row in dtInvert2.Rows)
                {
                    //need to set value to NewColumn column
                    row["Content"] = "Purchase Ratio";   // or set it to some other value
                    row["UOM"] = "%";
                    row["Department"] = "Plant Finanace";
                }

                //Adding inversion code for dtNOI
                DataTable dtInvert3 = new DataTable();
                dtInvert3 = GetInversedDataTable(dtNOI, "Plant", "");
                dtInvert3.Columns.Add("Content", typeof(System.String)).SetOrdinal(0);
                dtInvert3.Columns.Add("UOM", typeof(System.String)).SetOrdinal(1);
                dtInvert3.Columns.Add("Department", typeof(System.String)).SetOrdinal(2);
                foreach (DataRow row in dtInvert3.Rows)
                {
                    //need to set value to NewColumn column
                    row["Content"] = "No of Items > Des Inv Level";   // or set it to some other value
                    row["UOM"] = "Nos";
                    row["Department"] = "Plant Finanace";
                }

                //Adding inversion code for dtCustComp

                DataTable dtInvertCC = new DataTable();
                dtInvertCC = GetInversedDataTable(dtCustComp, "Plant", "");
                dtInvertCC.Columns.Add("Content", typeof(System.String)).SetOrdinal(0);
                dtInvertCC.Columns.Add("UOM", typeof(System.String)).SetOrdinal(1);
                dtInvertCC.Columns.Add("Department", typeof(System.String)).SetOrdinal(2);
                foreach (DataRow row in dtInvertCC.Rows)
                {
                    //need to set value to NewColumn column
                    row["Content"] = "Customer Complaints";   // or set it to some other value
                    row["UOM"] = "PPM";
                    row["Department"] = "QMS & QA";
                }
                //Adding inversion code for dtNCActual
                DataTable dtInvert4 = new DataTable();
                dtInvert4 = GetInversedDataTable(dtNCActual, "Plant", "");
                dtInvert4.Columns.Add("Content", typeof(System.String)).SetOrdinal(0);
                dtInvert4.Columns.Add("UOM", typeof(System.String)).SetOrdinal(1);
                dtInvert4.Columns.Add("Department", typeof(System.String)).SetOrdinal(2);
                foreach (DataRow row in dtInvert4.Rows)
                {
                    //need to set value to NewColumn column
                    row["Content"] = "NC Count";   // or set it to some other value
                    row["UOM"] = "Nos";
                    row["Department"] = "QMS & QA";
                }

                //Adding inversion code for dtNCTarget
                DataTable dtInvert5 = new DataTable();
                dtInvert5 = GetInversedDataTable(dtNCTarget, "Plant", "");
                dtInvert5.Columns.Add("Content", typeof(System.String)).SetOrdinal(0);
                dtInvert5.Columns.Add("UOM", typeof(System.String)).SetOrdinal(1);
                dtInvert5.Columns.Add("Department", typeof(System.String)).SetOrdinal(2);
                foreach (DataRow row in dtInvert5.Rows)
                {
                    //need to set value to NewColumn column
                    row["Content"] = "NC Count";   // or set it to some other value
                    row["UOM"] = "Nos";
                    row["Department"] = "QMS & QA";
                }

                //Adding inversion code for dtPremFrgt
                DataTable dtInvertPF = new DataTable();
                dtInvertPF = GetInversedDataTable(dtPreFrgt, "Plant", "");
                dtInvertPF.Columns.Add("Content", typeof(System.String)).SetOrdinal(0);
                dtInvertPF.Columns.Add("UOM", typeof(System.String)).SetOrdinal(1);
                dtInvertPF.Columns.Add("Department", typeof(System.String)).SetOrdinal(2);
                foreach (DataRow row in dtInvertPF.Rows)
                {
                    //need to set value to NewColumn column
                    row["Content"] = "Premimum Freight";   // or set it to some other value
                    row["UOM"] = "INR";
                    row["Department"] = "Plant/Marketing";
                }

                //Adding inversion code for dtHeadCount
                DataTable dtInvertHC = new DataTable();
                dtInvertHC = GetInversedDataTable(dtHeadCount, "Plant", "");
                dtInvertHC.Columns.Add("Content", typeof(System.String)).SetOrdinal(0);
                dtInvertHC.Columns.Add("UOM", typeof(System.String)).SetOrdinal(1);
                dtInvertHC.Columns.Add("Department", typeof(System.String)).SetOrdinal(2);
                foreach (DataRow row in dtInvertHC.Rows)
                {
                    //need to set value to NewColumn column
                    row["Content"] = "Head Count";   // or set it to some other value
                    row["UOM"] = "Nos";
                    row["Department"] = "HR";
                }

                //Adding inversion code for dtPitIde
                DataTable dtInvertPI = new DataTable();
                dtInvertPI = GetInversedDataTable(dtPitIde, "Plant", "");
                dtInvertPI.Columns.Add("Content", typeof(System.String)).SetOrdinal(0);
                dtInvertPI.Columns.Add("UOM", typeof(System.String)).SetOrdinal(1);
                dtInvertPI.Columns.Add("Department", typeof(System.String)).SetOrdinal(2);
                foreach (DataRow row in dtInvertPI.Rows)
                {
                    //need to set value to NewColumn column
                    row["Content"] = "PITCH IDEA";   // or set it to some other value
                    row["UOM"] = "Nos";
                    row["Department"] = "HR";
                }

                //Merging of all data tables
                dtInvertHC.Merge(dtInvertPI);
                dtInvertPF.Merge(dtInvertHC);
                dtInvert5.Merge(dtInvertPF);
                dtInvert4.Merge(dtInvert5);
                dtInvertCC.Merge(dtInvert4);
                dtInvert3.Merge(dtInvertCC);
                dtInvert2.Merge(dtInvert3);
                dtInvert.Merge(dtInvert2);

                GridView1.DataSource = dtInvert;
                GridView1.BackColor = System.Drawing.Color.White;
                GridView1.DataBind();
            }
            catch (Exception ex)
            {
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alertMessage", "alert(" + ex.Message + ")", true);
            }
            finally
            {
                constr.Close();
            }
        }

        private void LoadRMSale()
        {
            string query = "select MatTypeView.[Mat Type], MatTypeView.[Pur Percent] from MatTypeView";
            DataTable dtMatTyp = new DataTable();
            try
            {
                //Mat_Typ
                SqlCommand cmd = new SqlCommand(query, constr);
                constr.Open();
                SqlDataReader dr = cmd.ExecuteReader(CommandBehavior.CloseConnection);
                DataTable dtSch = dr.GetSchemaTable();
                dtMatTyp.Load(dr);

                double sum = 0, val = 0;
                DataTable dtInvert = new DataTable();
                dtInvert = GetInversedDataTable(dtMatTyp, "Mat Type", "");
                dtInvert.Columns.Add("Total");
                for (int i = 0; i < dtInvert.Rows.Count; i++)
                {
                    for (int j = 0; j < dtInvert.Columns.Count; j++)
                    {
                        if (j != 0)
                        {
                            sum = sum + DBNulls.NumberValue(dtInvert.Rows[i][j]);
                        }
                        val = j;
                    }
                    dtInvert.Rows[i][dtInvert.Columns.Count - 1] = sum;
                    sum = 0;
                }

                GridView2.DataSource = dtInvert;
                GridView2.DataBind();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                constr.Close();
            }
        }

        private void ReadSyncTime()
        {
            try
            {
                constr.Open();
                SqlCommand cmd = new SqlCommand("Select PrimaryColumn from tblSalesDb", constr);
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                SqlDataReader dr = cmd.ExecuteReader();
                dr.Read();
                Label1.Text = "Last Updated on: " + dr["PrimaryColumn"].ToString();
            }
            catch (Exception ex)
            {
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alertMessage", "alert(" + ex.Message + ")", true);
            }
            finally
            {
                constr.Close();
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            ReadSyncTime();
            LoadDDSales();
            LoadRMSale();
            Literal_Timeline();
        }

        protected void GridView1_PreRender(object sender, EventArgs e)
        {
            MergeRows(GridView1);
        }

        protected void Literal_Timeline()
        {
            //Literal1.Text = "<ul class='timeline-events'><li class='timeline-event-years-3 timeline-event-legend'></li><li class='timeline-event-years-2'><h2>2015 Sep</h2><h3>Crossed 25 cr</h3><h4 style='font-weight:bold; font-size:14px;'>26.49 cr</h4></li><li class='timeline-event-years-2'><h2 style='font-weight:bold;'>2017-18</h2><h3>Till Dec-2017</h3><h4 style='font-weight:bold; font-size:18px;'>31.26 cr</h4></li><li class='timeline-event-years-2'><h2 style='font-weight:bold;'>2018-19</h2><h3>Till Jan-2018</h3><h4 style='font-weight:bold; font-size:18px;'>32.04 cr</h4></li><li class='timeline-event-years-3'><h2 style='font-weight:bold;'>2018-19</h2><h3>Current Achievement</h3><h4 style='font-weight:bold; font-size:18px;'>33.10 cr</h4></li></ul><ul class='timelines-years'><li>2011</li><li>2013</li><li>2015</li><li>2017</li><li>2018</li><li>2019</li></ul><script src='http://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js'></script>";

            Literal1.Text = "<ul class='timeline-events'><li class='timeline-event-years-2'><h2>2015-16</h2><h3>Sep'15</h3><h4 style='font-weight:bold; font-size:14px;'>26.49 cr</h4></li><li class='timeline-event-years-2'><h2 style='font-weight:bold;'>2017-18</h2><h3>Sep'17</3><h4 style='font-weight:bold; font-size:18px;'>31.26 cr</h4></li><li class='timeline-event-years-2'><h2 style='font-weight:bold;'>2017-18</h2><h3>Dec'17</3><h4 style='font-weight:bold; font-size:18px;'>32.04 cr</h4></li><li class='timeline-event-years-2'><h2 style='font-weight:bold;'>2017-18</h2><h3>Jan'18</3><h4 style='font-weight:bold; font-size:18px;'>33.10 cr</h4></li><li class='timeline-event-years-2'><h2 style='font-weight:bold;'>2017-18</h2><h3>Mar'18</h3><h4 style='font-weight:bold; font-size:18px;'>34.53 cr</h4></li></ul><script src='http://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js'></script>";
        }

        protected void GridView1_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            //for (int x = 0; x < e.Row.Cells.Count; x++)
            //{
            //    // work out how wide the cell span should be for this cell
            //    int span = 1;
            //    while (

            //        (x + span < e.Row.Cells.Count) &&
            //        (e.Row.Cells[x + span].Text == e.Row.Cells[x].Text) &&
            //        (e.Row.Cells[x].Text != "&nbsp;")
            //        )
            //    {
            //        span++;
            //    }

            //    // if we need to span this cell
            //    if (span > 1)
            //    {
            //        // apply the span value to the first cell in the group
            //        e.Row.Cells[x].ColumnSpan = span;

            //        for (int n = 1; n < span; n++)
            //        {
            //            while (n < span)
            //            {
            //                e.Row.Cells[x + span - n].Visible = false;
            //                n++;
            //            }
            //        }
            //        // skip to the next cell past the end of the span
            //        x += span - 1;
            //    }
            //}
        }

        protected void ddlTest_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlTest.SelectedValue == "1") {

                Response.Redirect("frmLoginNew.aspx?type=premium");
            }
            else if (ddlTest.SelectedValue == "2")
            {

                Response.Redirect("frmLoginNew.aspx?type=customer");
            }
            else if (ddlTest.SelectedValue == "3")
            {

                Response.Redirect("frmLoginNew.aspx?type=head");
            }
            else if (ddlTest.SelectedValue == "4")
            {

                Response.Redirect("frmLoginNew.aspx?type=purchase");
            }
            else if (ddlTest.SelectedValue == "5")
            {

                Response.Redirect("frmLoginNew.aspx?type=pitch");
            }
            else if (ddlTest.SelectedValue == "0") {
                Response.Redirect("Daily_Dashboard.aspx");
            }
        }
    }
}