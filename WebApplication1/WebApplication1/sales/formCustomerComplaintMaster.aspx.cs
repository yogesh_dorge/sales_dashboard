﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WebApplication1.sales
{
    public partial class formCustomerComplaintMaster : System.Web.UI.Page
    {
        SqlConnection constr = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnectionString"].ToString());
        SqlDataAdapter adapt;
        DataTable dt;
        
        protected void Page_Load(object sender, EventArgs e)
        {
            
           
            if (!IsPostBack) {
                showData();
            
            }
        }

        protected void rgvCustomerComplaint_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
        {
            rgvCustomerComplaint.EditIndex = -1;
            showData();

        }

        protected void rgvCustomerComplaint_RowEditing(object sender, GridViewEditEventArgs e)
        {
            rgvCustomerComplaint.EditIndex = e.NewEditIndex;
            showData();

        }

        protected void rgvCustomerComplaint_RowUpdating(object sender, GridViewUpdateEventArgs e)
        {
            constr.Open();
            try
            {

                Label id = rgvCustomerComplaint.Rows[e.RowIndex].FindControl("lbl_Id") as Label;
                TextBox plant = rgvCustomerComplaint.Rows[e.RowIndex].FindControl("txt_plant") as TextBox;
                TextBox Actual = rgvCustomerComplaint.Rows[e.RowIndex].FindControl("txt_Actual") as TextBox;
                TextBox target = rgvCustomerComplaint.Rows[e.RowIndex].FindControl("txt_Target") as TextBox;
                SqlCommand cmd = new SqlCommand("Update tblDDCustomerComplaint set Actual='" + Actual.Text + "',Target='" + target.Text + "' where id='" + id.Text + "'", constr);
                cmd.ExecuteNonQuery();
                rgvCustomerComplaint.EditIndex = -1;
                constr.Close();
                showData();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                constr.Close();
            }
        }
        protected void showData() {
            try
            {
                dt = new DataTable();
                constr.Open();
                adapt = new SqlDataAdapter("select * from tblDDCustomerComplaint", constr);
                adapt.Fill(dt);
                if (dt.Rows.Count > 0)
                {
                    rgvCustomerComplaint.DataSource = dt;
                    rgvCustomerComplaint.DataBind();
                }

            }
            catch (Exception ex) { throw ex; }
            finally
            {
                constr.Close();
            }
        
        
        }
    }
}