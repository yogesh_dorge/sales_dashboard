﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
namespace WebApplication1.sales
{
    public partial class formHeadCountMaster : System.Web.UI.Page
    {
        SqlConnection constr = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnectionString"].ToString());
        SqlDataAdapter adapt;
        DataTable dt;

        protected void Page_Load(object sender, EventArgs e)
        {


            if (!IsPostBack)
            {
                showData();

            }
        }

        protected void rgvHeadCount_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
        {
            rgvHeadCount.EditIndex = -1;
            showData();
        }

        protected void rgvHeadCount_RowEditing(object sender, GridViewEditEventArgs e)
        {
            rgvHeadCount.EditIndex = e.NewEditIndex;
            showData();
        }

        protected void rgvHeadCount_RowUpdating(object sender, GridViewUpdateEventArgs e)
        {


            constr.Open();
            try
            {

                Label id = rgvHeadCount.Rows[e.RowIndex].FindControl("lbl_Id") as Label;
                TextBox plant = rgvHeadCount.Rows[e.RowIndex].FindControl("txt_plant") as TextBox;
                TextBox Actual = rgvHeadCount.Rows[e.RowIndex].FindControl("txt_Actual") as TextBox;
                TextBox target = rgvHeadCount.Rows[e.RowIndex].FindControl("txt_Target") as TextBox;
                TextBox Available = rgvHeadCount.Rows[e.RowIndex].FindControl("txt_Available") as TextBox;
                SqlCommand cmd = new SqlCommand("Update tblDDHeadCount set Actual='" + Actual.Text + "',Available='" + Available.Text+ "',Target='" + target.Text + "' where id='" + id.Text + "'", constr);
                cmd.ExecuteNonQuery();
                rgvHeadCount.EditIndex = -1;
                constr.Close();
                showData();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                constr.Close();
            }
        }
        protected void showData()
        {
            try
            {
                dt = new DataTable();
                constr.Open();
                adapt = new SqlDataAdapter("select * from tblDDHeadCount", constr);
                adapt.Fill(dt);
                if (dt.Rows.Count > 0)
                {
                    rgvHeadCount.DataSource = dt;
                    rgvHeadCount.DataBind();
                }

            }
            catch (Exception ex) { throw ex; }
            finally
            {
                constr.Close();
            }


        }
    }
}