﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Data.SqlClient;
using System.Data;
using System.Web.SessionState;
using System.Net.Mail;
using System.Net;
using Telerik.Web.UI;
using System.Globalization;
using System.Text.RegularExpressions;
using unirest_net.http;
using System.Web.Configuration;

namespace WebApplication1
{
    public partial class frmLoginNew : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            
        }
        public static string fnGetUsableRTC_sec(System.DateTime vDate)
        {
            return string.Format("{0:yyyyMMddHHmmss}", vDate);
        }
        protected void insertLog(string userName, string where) {
            try
            {
                string qry = "insert into tbl_Log_Master  ([username],[entry_type],[rtc]) values ('" + userName + "','" + where + "','" + fnGetUsableRTC_sec(DateTime.Now) + "')";
              int i=  DBUtils.ExecuteSQLCommand(new SqlCommand(qry));

            }
            catch (Exception ex) {
                throw ex;
            }
        
        
        }
        protected void Button1_Click(object sender, EventArgs e)
        {
            try
            {
                if (txtUsername.Text.Contains("@sanjeevgroup.com"))
                {
                
                }
                else
                {
                    txtUsername.Text = txtUsername.Text + "@sanjeevgroup.com";
                }
                bool result = false;
                bool result1 = false;
                HttpResponse<string> response = null;
                try
                {
                    response = Unirest.get("https://outlook.office365.com/api/v1.0/me/").basicAuth(txtUsername.Text, txtPassword.Text).asJson<string>();
                }
                catch (Exception ex)
                {
                    lblMessage.Text = "Login Failed";
                    return;
                }
                if (response.Code == 200)
                    result = true;
                if (result)
                {
                    string Name = response.Body.ToString();
                    string[] lines = Name.Split(new[] { ':' }, StringSplitOptions.RemoveEmptyEntries);
                    string line = lines[6].ToString();
                    string[] pair = line.Split(new[] { ',' });
                    string Login_Name = pair[0].Trim().ToString();
                    string qry;
                    System.Data.DataTable dt;
                    DateTime Today_date = DateTime.Now;
                    string Today_date_rtc = PublicMethods.fnGetUsableRTC_RTC(Today_date);

                    DataTable dtrole = new DataTable();
                    Session.Add("LoginUserEmail", txtUsername.Text);
                    Session.Add("Password", txtPassword.Text);

                    string type = Request.QueryString["type"];
                    string location = "";
                    if (type == "customer")
                    {
                         location = "Customer Complaint";
                        insertLog(txtUsername.Text, location);
                       // Response.Redirect("formCustomerComplaintMaster.aspx");
                        Response.Redirect("~/sales/formCustomerComplaintMaster.aspx", false);
                    }
                    else if (type == "purchase")
                    {
                        location = "Purchase Ration ";
                        insertLog(txtUsername.Text, location);
                        Response.Redirect("~/sales/TargetMaster.aspx", false);

                    }
                    else if (type == "head")
                    {
                        location = "Head Count ";
                        insertLog(txtUsername.Text, location);
                        Response.Redirect("~/sales/formHeadCountMaster.aspx", false);
                    }
                    else if (type == "pitch")
                    {
                        location = "Pitch Idea ";
                        insertLog(txtUsername.Text, location);
                        Response.Redirect("~/sales/formPitchIdeaMaster.aspx", false);
                    }
                    else if (type == "premium")
                    {
                        location = "Premium Freight ";
                        insertLog(txtUsername.Text, location);
                        Response.Redirect("~/sales/formPremiumFreightMaster.aspx", false);
                    }
                    else if (type == "calendar")
                    {
                        location = "Calendar";
                        insertLog(txtUsername.Text, location);
                        Response.Redirect("~/sales/frmHolidayMaster.aspx", false);
                    }
                    else if (type == "REV")
                    {
                        location = "REV";
                        insertLog(txtUsername.Text, location);
                        Response.Redirect("~/sales/frmREVMaster.aspx", false);
                    }
                    else if (type == "YEO")
                    {
                        location = "YEO";
                        insertLog(txtUsername.Text, location);
                        Response.Redirect("~/sales/frmYEOA_M_Master.aspx", false);
                    }
                    HttpContext.Current.ApplicationInstance.CompleteRequest();
                }
                else
                {
                    ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alertMessage", "alert('Email/Password is wrong')", true);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}