﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="~/sales/SaleDashboard.aspx.cs" Inherits="WebApplication1.SaleDashboard" %>

<html xmlns="http://www.w3.org/1999/xhtml">
<!-- Meta Tag -->
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<!-- SEO -->
<meta name="description" content="150 words">
<meta name="author" content="uipasta">
<meta name="url" content="http://www.yourdomainname.com">
<meta name="copyright" content="company name">
<meta name="robots" content="index,follow">
<meta http-equiv="refresh" content="900">
<head id="Head1" runat="server">
    <title>Sale Dashboard Live</title>
    <script type="text/javascript">
        function BatchEditCellValueChanged(sender, args) {


            var grid = sender;

            var row = args.get_row();
            var cell = args.get_cell();
            var tableView = args.get_tableView();
            var column = args.get_column();
            var columnUniqueName = args.get_columnUniqueName();
            var editorValue = args.get_editorValue();
            var cellValue = args.get_cellValue();
            alert(editorValue);

            var columnlength = grid.get_masterTableView().get_columns().length;
            for (i = 0 ; i < columnlength; i++) {
                cell = grid.get_masterTableView().get_columns()[i].get_uniqueName();
                alert(cell);
            }

        }
    </script>

    <!-- Favicon -->
    <link rel="shortcut icon" href="images/favicon/favicon.ico">
    <!-- All CSS Plugins -->
    <link rel="stylesheet" type="text/css" href="css/plugin.css">
    <!-- Main CSS Stylesheet -->
    <link rel="stylesheet" type="text/css" href="css/style.css">
    <!-- Timeline CSS Stylesheet -->
    <link rel="stylesheet" type="text/css" href="css/styleTimeline.css">
    <!-- Google Web Fonts  -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Poppins:400,300,500,600,700">
    <style type="text/css">
        .navbar-header {
            text-align: center;
        }
    </style>


    <style type="text/css">
        .RadGrid_WebBlue td, .RadGrid_WebBlue th {
            border-left: solid 1px black !important;
            border-right: solid 1px black !important;
            border-top: solid 1px black !important;
            border-bottom: solid 1px black !important;
            padding-left: 7px !important;
            padding-right: 7px !important;
            border-bottom: none !important;
            /*background-color:white !important;*/
        }

        .RadGrid_WebBlue {
            border-left: solid 1px black !important;
            border-right: solid 1px black !important;
        }

            .RadGrid_WebBlue .rgSelectedRow td {
                border-bottom: solid 1px black !important;
                padding-bottom: 2px !important;
            }
            .black_overlay{
                display:none;
                position: absolute;
                top: 0%;
                left: 0%;
                width: 100%;
                height: 100%;
                background-color:black;
                z-index:1001;
                -moz-opacity: 0.8;
                opacity:.80;
                filter: alpha(opacity=80);
            }
            .white_content {
                display:none;
                position: absolute;
                top: 25%;
                left: 35%;
                width: 35%;
                padding: 0px;
                border: 0px solid #5c90c2;
                background-color: white;
                z-index:1002;
                overflow: auto;
            }
            .headertext{
                font-family:Arial, Helvetica, sans-serif;
                font-size:14px;
                color:#f19a19;
                font-weight:bold;
            }
            .textfield
            {
                border:1px solid #a6c25c;
                width:135px;
            }
            .button2
            {
                background-color:#5c90c2;
                color:White;
                font-size:11px;
                font-weight:bold;
                border:1px solid #7f9db9;
                width:68px;
            }
    </style>

</head>
<body>
    <form id="form1" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server">
            <Scripts>
                <asp:ScriptReference Name="jquery" />
                <asp:ScriptReference Name="jquery.ui.combined" />
            </Scripts>
        </asp:ScriptManager>
        <%--<telerik:RadSkinManager ID="RadSkinManager1" runat="server" ShowChooser="True"></telerik:RadSkinManager>--%>

        <!--Menu section Start-->
        <div style="width: 100%;background:white; padding-bottom:80px;">
            <table class="col-lg-11">
                <tr>
                    <td style="width: 14%">
                       <%-- <img src="../Images/sanjeev.png" />--%>
                         <img src="../Images/logo.png" />
                    </td>
                    <td style="color: green;margin-left:370px">
                        <h2>Daily Sales Monitoring
                        </h2>
                    </td>
                    <td>
                         <asp:dropdownlist Width="140%" Font-Size="20px" runat="server" id="ddlTest" OnSelectedIndexChanged="ddlTest_SelectedIndexChanged" AutoPostBack="true" > 
                             <asp:listitem text="Select Master" value="0"></asp:listitem>
     <asp:listitem text="Calendar" value="1"></asp:listitem>
     <asp:listitem text="REV YEO" value="2"></asp:listitem>
     <asp:listitem text="YEO Avg/Mnth" value="3"></asp:listitem>
</asp:dropdownlist>
                    </td>
                </tr>
            </table>
        </div>
        <!--Menu section End-->
        <div id="light" class="white_content">
<table cellpadding=0 cellspacing=0 border=0 style="background-color:#5c90c2;" width="100%">
	<tr>
		<td height="16px">
			<a href = "javascript:void(0)" onclick = "document.getElementById('light').style.display='none';document.getElementById('fade').style.display='none'">
				<img src="close.gif" style="border :0px"  width="13px" align="right" height="13px"/>
			</a>
		</td>
	</tr>
    <tr>
        <td style="padding-left:16px;padding-right:16px;padding-bottom:16px"> 
			<table align="center"  border="0" cellpadding="0" cellspacing="0" style="background-color:#fff" width="100%">
				<tr>
					<td align="center" colspan="2" class="headertext" >Login Form </td>
				</tr>
				<tr>
					<td>&nbsp;</td>
				</tr>
				<tr>
					<td align="center">
						<table>
							<tr>
								<td align="right">Username:</td>
									<td>
										<asp:TextBox ID="txtUsername" runat="server" CssClass="textfield">
										</asp:TextBox>@sanjeevgroup.com
									</td>
                            </tr>
							<tr>
								<td height="10px"></td>
							</tr>
							<tr>
								<td align="right">Password:</td>
									<td>
										<asp:TextBox ID="txtPassword" runat="server" CssClass="textfield" TextMode="Password">
										</asp:TextBox>
									</td>
							</tr>
							<tr>
								<td height="10px"></td>
							</tr>
							<tr>
								<td><asp:Label ID="lblMessage" runat="server" Text="" Visible="false" ForeColor="Red"></asp:Label></td>
								<td><asp:Button ID="Button1" runat="server" Text="Login" OnClick="Button1_Click" />
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td height="10px"></td>
				</tr>
			</table>
		</td>
	</tr>
</table>
            
<div align="center" class=" headertext">
<asp:Label ID="txtlbl" runat="server"  ></asp:Label>
</div>
</div>
<div id="fade" class="black_overlay"></div>
        <!-- About Start -->

        <section id="about" class="about section-space-padding">
            <div class="container">
                <div class="row">
                </div>
            </div>
            <div class="modal fade subscribe padding-top-120" id="subscribemodal" role="dialog">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-body">
                            <div class="row">
                                <div class="col-sm-offset-2 col-xs-offset-0 col-md-8 col-sm-8">
                                    <div class="margin-bottom-50">
                                        <form id="mc-form" method="post" action="http://uipasta.us14.list-manage.com/subscribe/post?u=854825d502cdc101233c08a21&amp;id=86e84d44b7">
                                            <div class="subscribe-form">
                                                <input id="mc-email" type="email" placeholder="Email Address" class="text-input">
                                                <button class="submit-btn" type="submit">Submit</button>
                                            </div>
                                            <label for="mc-email" class="mc-label"></label>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- About End-->
            <div class="container">
                <div class="row">
                    <div class="col-lg-4">
                        <telerik:RadTextBox ID="rdWorkingDays" runat="server" ReadOnly="true" Width="200px"></telerik:RadTextBox>
                        <h5><b>Plantwise Sales Summary</b></h5>

                        <telerik:RadGrid ID="RadGridPlant" runat="server" AutoGenerateColumns="false" Font-Size="Large" GridLines="Both" Skin="Simple" OnItemDataBound="RadGridPlant_ItemDataBound"  AlternatingItemStyle-BackColor="White"  >
                            <ClientSettings>
                                <ClientEvents OnBatchEditCellValueChanged="BatchEditCellValueChanged" />
                            </ClientSettings>
                            <MasterTableView TableLayout="Fixed">
                                <Columns>
                                    <telerik:GridNumericColumn Aggregate="None" DataField="Plant" DataType="System.String" HeaderText="Plant" SortExpression="Plant" UniqueName="Plant">
                                        <HeaderStyle Width="100px" BackColor="#0099ff"   ForeColor="White"   Font-Bold="true"     />
                                     
                                    </telerik:GridNumericColumn>
                                    <telerik:GridNumericColumn Aggregate="None" DataField="C4" DataType="System.Decimal" HeaderText="C4" SortExpression="C4" UniqueName="C4">
                                        <HeaderStyle Width="35px"  ForeColor="White"    BackColor="#0099ff" Font-Bold="true"  />
                                      
                                    </telerik:GridNumericColumn>
                                    <telerik:GridNumericColumn Aggregate="None" DataField="PNST" DataType="System.Decimal" HeaderText="E89" SortExpression="E89" UniqueName="E89">
                                        <HeaderStyle Width="35px"   ForeColor="White"   BackColor="#0099ff" Font-Bold="true"  />
                                      
                                    </telerik:GridNumericColumn>
                                    <telerik:GridNumericColumn Aggregate="None" DataField="K96" DataType="System.Decimal" HeaderText="K96" SortExpression="K96" UniqueName="K96">
                                        <HeaderStyle Width="35px"   ForeColor="White"    BackColor="#0099ff" Font-Bold="true"  />
                                      
                                    </telerik:GridNumericColumn>
                                    <telerik:GridNumericColumn Aggregate="None" DataField="P115" DataType="System.Decimal" HeaderText="P115" SortExpression="P115" UniqueName="P115">
                                        <HeaderStyle Width="35px"   ForeColor="White"   BackColor="#0099ff" Font-Bold="true"  />
                                      
                                    </telerik:GridNumericColumn>
                                    <telerik:GridNumericColumn Aggregate="None" DataField="X1" DataType="System.Decimal" HeaderText="X1" SortExpression="X1" UniqueName="X1">
                                        <HeaderStyle Width="40px"  ForeColor="White"    BackColor="#0099ff" Font-Bold="true"  />
                                      
                                    </telerik:GridNumericColumn>
                                    <telerik:GridNumericColumn Aggregate="None" DataField="Total" DataType="System.Decimal" HeaderText="Total" SortExpression="Total" UniqueName="Total">
                                        <HeaderStyle Width="40px"  ForeColor="White"     BackColor="#0099ff" Font-Bold="true"  />
                                      
                                    </telerik:GridNumericColumn>
                                </Columns>
                            </MasterTableView>
                        </telerik:RadGrid>
                        
                    </div>

                    <div class="col-lg-4">
                        <h5>
                            <b>K96 Plant Sale:=></b>
                        </h5>
                        <telerik:RadGrid ID="RadGrid1" runat="server" AutoGenerateColumns="false" AllowSorting="true" Font-Size="Medium" Skin="Simple" GridLines="Both" ShowFooter="true" OnPreRender="RadGrid1_PreRender" AlternatingItemStyle-BackColor="White" >
                            <MasterTableView TableLayout="Fixed">
                                <Columns>
                                    <telerik:GridNumericColumn Aggregate="None" DataField="Customer" DataType="System.String" HeaderText="K96" SortExpression="Customer Name" UniqueName="Customer_Name">
                                        <HeaderStyle Width="30px"  ForeColor="White"    BackColor="#0099ff" Font-Bold="true"  />
                                      
                                    </telerik:GridNumericColumn>
                                    <telerik:GridNumericColumn Aggregate="None" DataField="Code" DataType="System.String" HeaderText="Cust Code" SortExpression="Cust Code" UniqueName="CustCode">
                                        <HeaderStyle Width="30px"  ForeColor="White"     BackColor="#0099ff" Font-Bold="true"  />
                                      
                                    </telerik:GridNumericColumn>
                                    <telerik:GridNumericColumn Aggregate="Sum" DataField="Plan" DataType="System.Decimal" HeaderText="Plan" SortExpression="Plan" UniqueName="Plan" FooterAggregateFormatString="{0}">
                                        <HeaderStyle Width="20px"  ForeColor="White"     BackColor="#0099ff" Font-Bold="true"  />
                                      
                                    </telerik:GridNumericColumn>
                                    <telerik:GridNumericColumn Aggregate="Sum" DataField="Actual" DataType="System.Decimal" HeaderText="Actual" SortExpression="Actual" UniqueName="Actual" FooterAggregateFormatString="{0}">
                                        <HeaderStyle Width="20px"  ForeColor="White"    BackColor="#0099ff" Font-Bold="true"  />
                                      
                                    </telerik:GridNumericColumn>
                                    <telerik:GridNumericColumn Aggregate="Sum" DataField="Gap" DataType="System.Decimal" HeaderText="Gap" SortExpression="Gap" UniqueName="Gap" FooterAggregateFormatString="{0}">
                                        <HeaderStyle Width="20px"  ForeColor="White"     BackColor="#0099ff" Font-Bold="true"  />
                                      
                                    </telerik:GridNumericColumn>
                                </Columns>
                            </MasterTableView>
                        </telerik:RadGrid>
                        <h5>
                            <b>P115 Plant Sale:=></b>
                        </h5>
                        <telerik:RadGrid ID="RadGrid2" runat="server" AutoGenerateColumns="false" AllowSorting="true" Font-Size="Medium" GridLines="Both" Skin="Simple" ShowFooter="true" AlternatingItemStyle-BackColor="White" >
                            <MasterTableView TableLayout="Fixed">
                                <Columns>
                                    <telerik:GridNumericColumn Aggregate="None" DataField="Customer" DataType="System.String" HeaderText="P115" SortExpression="Customer Name" UniqueName="Customer_Name" FooterAggregateFormatString="{0}">
                                        <HeaderStyle Width="30px"  ForeColor="White"    BackColor="#0099ff" Font-Bold="true"  />

                                      
                                    </telerik:GridNumericColumn>
                                    <telerik:GridNumericColumn Aggregate="Sum" DataField="Plan" DataType="System.Decimal" HeaderText="Plan" SortExpression="Plan" UniqueName="Plan" FooterAggregateFormatString="{0}">
                                        <HeaderStyle Width="20px"  ForeColor="White"    BackColor="#0099ff" Font-Bold="true"  />
                                      
                                    </telerik:GridNumericColumn>
                                    <telerik:GridNumericColumn Aggregate="Sum" DataField="Actual" DataType="System.Decimal" HeaderText="Actual" SortExpression="Actual" UniqueName="Actual" FooterAggregateFormatString="{0}">
                                        <HeaderStyle Width="20px"  ForeColor="White"      BackColor="#0099ff" Font-Bold="true"  />
                                      
                                    </telerik:GridNumericColumn>
                                    <telerik:GridNumericColumn Aggregate="Sum" DataField="Gap" DataType="System.Decimal" HeaderText="Gap" SortExpression="Gap" UniqueName="Gap" FooterAggregateFormatString="{0}">
                                        <HeaderStyle Width="20px"  ForeColor="White"    BackColor="#0099ff" Font-Bold="true"  />
                                      
                                    </telerik:GridNumericColumn>
                                </Columns>
                            </MasterTableView>
                        </telerik:RadGrid>
                    </div>
                    <div class="col-lg-4">
                    </div>
                    <div class="col-lg-4">
                        <h5>
                            <b>Bajaj Spares in Lacs:=></b>
                        </h5>
                        <telerik:RadGrid runat="server" ID="RadGrid3" AllowSorting="true" Font-Size="Large"  HeaderStyle-BackColor="#0099ff" AlternatingItemStyle-BackColor="White" HeaderStyle-ForeColor="White" HeaderStyle-Font-Bold="true"  Skin="Simple" >
                        </telerik:RadGrid>
                        <h5>
                            <b>SOB Part wise Sale:=></b>
                        </h5>
                        <telerik:RadGrid runat="server" ID="RadGrid4" AutoGenerateColumns="false" AllowSorting="true" Font-Size="Medium" Skin="Simple" GridLines="Both" OnItemDataBound="RadGrid4_ItemDataBound" AlternatingItemStyle-BackColor="White" >
                            <MasterTableView TableLayout="Fixed">
                                <Columns>
                                    <telerik:GridNumericColumn Aggregate="None" DataField="Part" DataType="System.String" HeaderText="Part" SortExpression="Part" UniqueName="Part">
                                        <HeaderStyle Width="80px" ForeColor="White"  BackColor="#0099ff"  Font-Bold="true" />
                                        
                                    </telerik:GridNumericColumn>
                                    <telerik:GridNumericColumn Aggregate="None" DataField="Customer Code" DataType="System.String" HeaderText="Cust" SortExpression="Cust" UniqueName="Cust">
                                        <HeaderStyle Width="50px"   ForeColor="White"  BackColor="#0099ff" Font-Bold="true" />
                                      
                                    </telerik:GridNumericColumn>
                                    <telerik:GridNumericColumn Aggregate="None" DataField="Desc" DataType="System.String" HeaderText="Desc" SortExpression="Desc" UniqueName="Description">
                                        <HeaderStyle Width="100px"  ForeColor="White"   BackColor="#0099ff" Font-Bold="true" />
                                    </telerik:GridNumericColumn>
                                    <telerik:GridNumericColumn Aggregate="None" DataField="Per Day Reqd" DataType="System.String" HeaderText="Per Day Reqd" SortExpression="Per Day Reqd" UniqueName="PerDayReqd">
                                        <HeaderStyle Width="40px"  ForeColor="White"   BackColor="#0099ff" Font-Bold="true" />
                                       
                                    </telerik:GridNumericColumn>
                                    <telerik:GridNumericColumn Aggregate="Sum" DataField="YestDay Sale Qty" DataType="System.Decimal" HeaderText="Yest Sale Qty" SortExpression="Yesterday Sale Qty" UniqueName="YesterdaySaleQty">
                                        <HeaderStyle Width="40px"  ForeColor="White"   BackColor="#0099ff" Font-Bold="true" />
                                       
                                    </telerik:GridNumericColumn>
                                    <telerik:GridNumericColumn Aggregate="Sum" DataField="Average" DataType="System.Decimal" HeaderText="Avg" SortExpression="Average" UniqueName="Average">
                                        <HeaderStyle Width="40px"   ForeColor="White"   BackColor="#0099ff" Font-Bold="true"/>
                                       
                                    </telerik:GridNumericColumn>
                                </Columns>
                            </MasterTableView>
                        </telerik:RadGrid>
                    </div>
                </div>
            </div>
        </section>

        
        
       
        <!-- Footer Start -->
       <%-- <footer class="footer-section">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">

                    </div>
                </div>
            </div>
        </footer>--%>
         <div style="position:relative; height:auto;">
            <asp:Literal ID="Literal1" runat="server"></asp:Literal>
        </div>
                                  <div>
         <hr />
        <center> <asp:Label ID="Label1" ForeColor="White" Font-Size="30px" runat="server" Text="Label"></asp:Label></div></center> 
        <!-- Footer End -->
        <!-- Back to Top Start -->
        <a href="#" class="scroll-to-top"><i class="icon-arrow-up-circle"></i></a>
        <!-- Back to Top End -->
      
        <!-- All Javascript Plugins  -->
        <script type="text/javascript" src="js/jquery.min.js"></script>
        <script type="text/javascript" src="js/plugin.js"></script>
        <!-- Main Javascript File  -->
        <script type="text/javascript" src="js/scripts.js"></script>
    </form>
</body>
</html>
