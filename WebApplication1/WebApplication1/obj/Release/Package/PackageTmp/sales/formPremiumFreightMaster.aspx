﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="formPremiumFreightMaster.aspx.cs" Inherits="WebApplication1.sales.formPremiumFreightMaster" %>

<html xmlns="http://www.w3.org/1999/xhtml">
    <!-- Meta Tag -->
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">    
    <meta http-equiv="X-UA-Compatible" content="IE=edge">    
    <!-- SEO -->
    <meta name="description" content="150 words">
    <meta name="author" content="uipasta">
    <meta name="url" content="http://www.yourdomainname.com">
    <meta name="copyright" content="company name">
    <meta name="robots" content="index,follow">
    <meta http-equiv="refresh" content="900">
    <head id="Head1" runat="server">
    <title>Premium Freight Master</title>
        <script type="text/javascript">
            function BatchEditCellValueChanged(sender, args) {

                var grid = sender;

                var row = args.get_row();
                var cell = args.get_cell();
                var tableView = args.get_tableView();
                var column = args.get_column();
                var columnUniqueName = args.get_columnUniqueName();
                var editorValue = args.get_editorValue();
                var cellValue = args.get_cellValue();
                alert(editorValue);

                var columnlength = grid.get_masterTableView().get_columns().length;
                for (i = 0 ; i < columnlength; i++) {
                    cell = grid.get_masterTableView().get_columns()[i].get_uniqueName();
                    alert(cell);
                }
            }
    </script>

    <!-- Favicon -->
    <link rel="shortcut icon" href="images/favicon/favicon.ico">
    <!-- All CSS Plugins -->
    <link rel="stylesheet" type="text/css" href="css/plugin.css">
    <!-- Main CSS Stylesheet -->
    <link rel="stylesheet" type="text/css" href="css/style.css">
    <!-- Google Web Fonts  -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Poppins:400,300,500,600,700">
        <style type="text/css">
            .navbar-header {
                text-align: center;
            }
        </style>
</head>
    <body>
        <form id="form1" runat="server">
            <asp:ScriptManager ID="ScriptManager1" runat="server">
            <Scripts>
                <asp:ScriptReference Name="jquery" />
                <asp:ScriptReference Name="jquery.ui.combined" />
            </Scripts>
        </asp:ScriptManager>
            <!--Menu section Start-->
            <div style=" width:100%; padding-bottom:80px;" >
                <table class="col-lg-11">
                    <tr>
                        <td style=" width: 14%">
                       <%-- <img src="../Images/sanjeev.png" />--%>
                         <img src="../Images/logo.png" />
                    </td>
                        <td>

                        </td>
                        <td>
                            <ul class="nav navbar-nav navbar-right">
                                <li>
                                    <a  href="Daily_Dashboard.aspx">Daily Dashboard</a>
                                </li>
                            </ul>
                        </td>
                    </tr>
                </table>
            </div>
            <!--Menu section End-->

            <!-- About Start -->

        <section id="about" class="about section-space-padding">
            <div class="container">                
                <div class="row">

                </div>
            </div>
            <div class="modal fade subscribe padding-top-120" id="subscribemodal" role="dialog">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-body">
                            <div class="row">
                                <div class="col-sm-offset-2 col-xs-offset-0 col-md-8 col-sm-8"><div class="margin-bottom-50">
                    <%--<form id="mc-form" method="post" action="http://uipasta.us14.list-manage.com/subscribe/post?u=854825d502cdc101233c08a21&amp;id=86e84d44b7">	
						  <div class="subscribe-form">
							 <input id="mc-email" type="email" placeholder="Email Address" class="text-input">
							  <button class="submit-btn" type="submit">Submit</button>
								</div>
								<label for="mc-email" class="mc-label"></label>
							  </form>--%>
                           </div>
                        </div>
                     </div>
                 </div>
             </div>
          </div>
       </div>
            <!-- About End-->
            <center><h1 style="margin: inherit;">Premium Freight Master</h1></center>
            <hr />
            <div class="container">
                <div class="row">
                    <div>

                            <asp:GridView ID="rgvPremiumFreight" runat="server" Align="Center" AutoGenerateColumns="false" CellPadding="6" OnRowCancelingEdit="rgvPremiumFreight_RowCancelingEdit" OnRowEditing="rgvPremiumFreight_RowEditing" OnRowUpdating="rgvPremiumFreight_RowUpdating">
                                <Columns>
                                    <asp:TemplateField>
                                        <ItemTemplate>
                                            <asp:Button ID="btn_Edit" runat="server" Text="Edit" CommandName="Edit" />
                                        </ItemTemplate>
                                        <EditItemTemplate>
                                            <asp:Button ID="btn_Update" runat="server" Text="Update" CommandName="Update"/>
                                            <asp:Button ID="btn_Cancel" runat="server" Text="Cancel" CommandName="Cancel"/>
                                        </EditItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="ID" Visible="false">
                                        <ItemTemplate>  
                                            <asp:Label ID="lbl_ID" runat="server" Text='<%#Eval("id") %>'></asp:Label>  
                                        </ItemTemplate>  
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Plant">  
                                        <ItemTemplate>
                                            <asp:Label ID="lbl_plant" runat="server" Text='<%#Eval("Plant") %>'></asp:Label>
                                        </ItemTemplate>
                                        <%--<EditItemTemplate>  
                                            <asp:TextBox ID="txt_plant" runat="server" Text='<%#Eval("Plant") %>' ReadOnly="true"></asp:TextBox>
                                        </EditItemTemplate>--%>
                                    </asp:TemplateField>  
                                    <asp:TemplateField HeaderText="Air">  
                                        <ItemTemplate>  
                                            <asp:Label ID="lbl_Air" runat="server" Text='<%#Eval("Air") %>'></asp:Label>  
                                        </ItemTemplate>  
                                        <EditItemTemplate>  
                                            <asp:TextBox ID="txt_Air" runat="server" Text='<%#Eval("Air") %>'></asp:TextBox>  
                                        </EditItemTemplate>  
                                    </asp:TemplateField>
                                     <asp:TemplateField HeaderText="Domestic">  
                                        <ItemTemplate>  
                                            <asp:Label ID="lbl_Domestic" runat="server" Text='<%#Eval("Domestic") %>'></asp:Label>  
                                        </ItemTemplate>  
                                        <EditItemTemplate>  
                                            <asp:TextBox ID="txt_Domestic" runat="server" Text='<%#Eval("Domestic") %>'></asp:TextBox>  
                                        </EditItemTemplate>  
                                    </asp:TemplateField>
                                </Columns>
                                <HeaderStyle BackColor="#663300" ForeColor="#ffffff"/>  
                            </asp:GridView>
              </div>
                    </div>
                </div>
        </section>

            <div class="container">
                <div class="row">
                    <div class="col-lg-10">
                        
              </div>
                    </div>
                </div>
        <!-- Footer Start -->
        <footer class="footer-section">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                    </div>
                </div>
            </div>
        </footer>
        <!-- Footer End -->
        <!-- Back to Top Start -->
        <a href="#" class="scroll-to-top"><i class="icon-arrow-up-circle"></i></a>
        <!-- Back to Top End -->
        <!-- All Javascript Plugins  -->
        <script type="text/javascript" src="js/jquery.min.js"></script>
        <script type="text/javascript" src="js/plugin.js"></script>
        <!-- Main Javascript File  -->
        <script type="text/javascript" src="js/scripts.js"></script>
        </form>
    </body>
</html>