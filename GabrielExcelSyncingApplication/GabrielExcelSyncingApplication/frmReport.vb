﻿Imports vtSqlConnection.vtWindows
Imports System.IO
Imports System.Xml
Imports System.Configuration
Imports Telerik.WinControls.UI
Imports Telerik.WinControls
Imports System.Net.NetworkInformation
Imports System.Management
Imports Microsoft.Office.Interop
Imports System.Text.RegularExpressions
Imports System.Net.Mail
Imports System.Linq
Imports DocumentFormat.OpenXml.Packaging
Imports DocumentFormat.OpenXml.Spreadsheet
Imports System.Data.SqlClient

Public Class frmReport
    Dim eApp As Excel.Application
    Dim eBook As Excel.Workbook
    Dim eSheet As Excel.Worksheet
    Dim eCell As Excel.Range
    Dim sheetName As String = String.Empty
    Dim columnstring As String = String.Empty
    Dim increment As Integer = 0
    Dim duplicateCounter As Integer = 0
    Dim successfullCount As Integer = 0
    Dim TotalCount As Integer = 0
    Dim unSucessfullCount As Integer = 0
    Dim RTC As String = String.Empty

    Private Sub frmReport_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Load

        Try
            CommonMethods.fnWriteStartAndEndLog()
            CommonMethods.fnWriteErrorLog("frmReport_Load", "Application started on '" + DateTime.Now + "'")
            Me.Show()
            fnSetProgressBar()
            connectToDatabase()

            ' New Patch for Log
            Dim synDate As DateTime = DateTime.Now.ToString("dd/MMM/yyyy HH:mm:ss tt")
            RTC = CommonMethods.fnDateTimeRTC(synDate)
            Dim que = "Insert into tbl_Sync_File_Log (DateTime,DateTimeRTC) Values ('" & DateTime.Now.ToString("dd/MMM/yyyy HH:mm:ss tt") & "','" & RTC & "')"
            Sql.ExecuteSql(que)

            Dim query = "Select * from tbl_Sync_FileManager"
            Dim dt As New DataTable()
            dt = Sql.getDatatable(query)
            For Each dr As DataRow In dt.Rows
                duplicateCounter = 0
                successfullCount = 0
                TotalCount = 0
                Dim que1 = String.Empty
                If (dr("File_Name").ToString().Contains(".xls") Or dr("File_Name").ToString().Contains(".xlsx") Or dr("File_Name").ToString().Contains(".XLSX") Or dr("File_Name").ToString().Contains(".XLS")) Then
                    'fnLoadExcelDetails(dr)
                    CommonMethods.fnWriteErrorLog("In '" + dr("File_Name") + "'", "Excel file to be synced")
                    Dim actualFileName As String
                    actualFileName = dr("File_Name")
                    dr("File_Name") = actualFileName
                    fnLoadExcelDetails1(dr)
                    CommonMethods.fnWriteEmailLog(DBNulls.StringValue(dr("File_Name")), DBNulls.StringValue(successfullCount), DBNulls.StringValue(duplicateCounter), duplicateCounter)
                    CommonMethods.fnWriteErrorLog("End of '" + dr("File_Name") + "'", "Excel file to be synced")
                    If successfullCount > 0 Then
                        que1 = "Insert into tbl_Sync_File_Log(DateTime,DateTimeRTC,Table_Name) Values ('" & synDate.ToString("dd/MMM/yyyy HH:mm:ss tt") & "','" & RTC & "','" & DBNulls.StringValue(dr("File_Name")) & "')"
                        Sql.ExecuteSql(que1)
                    End If
                End If
            Next
            'fnSetEmailNotification()
            '*******New Patch for File moving*******
            'fnSimpleMove("sales_db.xls")
            'fnSimpleMove("Purch_percnt_db")
            'fnSimpleMove("Purch_ratio_db")
            'fnSimpleMove("ymsales_db")


            ProgressBar1.Value = 100
            If (ProgressBar1.Value >= ProgressBar1.Maximum) Then
                Me.Close()
                CommonMethods.fnWriteErrorLog("frmReport_Load", "Application exited on '" + DateTime.Now + "'")
                CommonMethods.fnWriteStartAndEndLog()
            End If

        Catch ex As Exception
            CommonMethods.fnWriteErrorLog("frmReport_Load", ex.Message)
            CommonMethods.fnWriteErrorLog("frmReport_Load", "Application exited on '" + DateTime.Now + "'")
            CommonMethods.fnWriteStartAndEndLog()
        End Try
    End Sub

    'Public Function LoadYSale()
    '    'Dim constr As String = "Data Source=192.168.1.45\SQLEXPRESS;Initial Catalog=dbSaleSummary;User ID=sa;Password=vspl@123"
    '    Try
    '        Dim constr As String = "Data Source=YOGESH_DORGE\SQLEXPRESS;Initial Catalog=dbSaleSummary;User ID=sa;Password=12345"
    '        Dim conn As New SqlConnection(constr)
    '        conn.Open()
    '        Dim b As SqlDataAdapter = New SqlDataAdapter("", conn)
    '        Dim a As SqlDataAdapter = New SqlDataAdapter("select PrimaryColumn as Ydate, Col14 as YSale,Col1 as Plant from tblSalesDb", conn)
    '        Dim dt As New DataTable
    '        a.Fill(dt)

    '        If dt.Rows.Count > 0 Then
    '            'SqlBulkCopy slqBulk = New SqlBulkCopy(conn)
    '            Dim SqlBulk As SqlBulkCopy = New SqlBulkCopy(conn)
    '            SqlBulk.DestinationTableName = "dbo.tblYesterday_Sale"
    '            'conn.Open()
    '            SqlBulk.WriteToServer(dt)
    '            conn.Close()
    '            End If
    '    Catch ex As Exception
    '        CommonMethods.fnWriteErrorLog("fnLoadYsale", ex.Message)
    '        CommonMethods.fnWriteErrorLog("frmReport_Load", "Application exited on '" + DateTime.Now + "'")
    '    End Try
    'End Function

    Dim spreadSheet As SpreadsheetDocument
    Dim newWorksheetPart As WorksheetPart
    Dim sheets As Sheets
    Dim relationshipId As String
    Dim sheetId As UInteger = 1
    Dim sheet As Sheet
    'Dim sheetName As String

    Private Sub connectToDatabase()
        Try
            Dim connString As String
            connString = CommonMethods.fnConnectionString()
            Sql.DatabaseType = Sql.EN_DatabaseType.SqlServer
            Sql.ConnectionString = connString
            Sql.fnInitialize()
        Catch ex As Exception
            CommonMethods.fnWriteErrorLog("connectToDatabase", ex.Message)
        End Try
    End Sub

    Private Sub fnLoadExcelDetails1(dr As DataRow)
        Try
            fnSetProgressBar()
            eApp = New Excel.Application
            Dim loc As String = dr("File_Path")
            'CommonMethods.fnGetFilePath(dr("File_Name"))
            eBook = eApp.Workbooks.Open(loc & "\" + dr("File_Name"))
            'eBook = InsertWorksheet(loc & "" + dr("File_Name") + "")
            CommonVariables.ColumnStart = dr("Column_Start")
            Dim flag As Integer = 0
            eSheet = eBook.Worksheets(1)
            'eSheet = newWorksheetPart
            eCell = eSheet.UsedRange

            Dim i As Integer = eSheet.UsedRange.Rows.Count
            Dim j As Integer = eSheet.UsedRange.Columns.Count

            Dim data = eCell.Value2

            Dim dt As DataTable = New DataTable()
            Dim dtMerge As DataTable = New DataTable()

            'DateTimeColumn
            'dt.Columns.Add("DateT")
            For index As Integer = 0 To j - 1
                dt.Columns.Add("Col" + (index + 1).ToString())
            Next

            For index As Integer = 0 To i - 1
                dt.Rows.Add()
                'First col as date
                'dt.Rows(index)(0) = Now

                For index1 As Integer = 0 To j - 1
                    If Not IsNothing(data(index + 1, index1 + 1)) Then
                        Dim str As String = data(index + 1, index1 + 1)
                        If Not str.Equals(String.Empty) Then
                            If str.EndsWith("-") Then
                                str = str.Trim()
                                str = str.Replace("-", "")
                                dt.Rows(index)(index1) = "-" + str
                            Else
                                dt.Rows(index)(index1) = str
                            End If

                        End If
                    End If
                Next
            Next
            fnSetProgressBar()
            Dim isTableCreated As String = "False"
            dt = fnMergeData(dt, dr("No_of_Columns").ToString(), DBNulls.StringValue(dr("Table_Name").ToString()))
            dt = fnChangeStringFormat(DBNulls.StringValue(dr("File_Name")), dt)
            'End If
            isTableCreated = fnTableCreate(dr("Table_Name").ToString(), Convert.ToInt16(dr("No_of_Columns")), dr("File_Name"), dt)

            If isTableCreated = "True" Then
                Try

                    'If (dr("Table_Name").ToString() = "ET_VEND_OTHERS" Or dr("Table_Name").ToString() = "ET_PO_HEADER" Or dr("Table_Name").ToString() = "tbl_Sync_Material") Then
                    '    Sql.BeginTran()
                    '    fnDelete(dr("Table_Name").ToString())
                    '    fnInsert(dr("Table_Name").ToString(), dt)
                    '    fnInsertLog(dr("Table_Name").ToString() + "_Log", dt)

                    '    Sql.CommitTran()
                    'Else
                    Sql.BeginTran()
                    'If (dr("Table_Name").ToString() = "ET_VEND_DETAIL") Then
                    '    fnInsertAdmin(dr("Table_Name").ToString())
                    '    fnInsertVendorCode(dr("Table_Name").ToString(), dt)
                    'End If
                    fnDelete(dr("Table_Name").ToString())
                    fnInsert(dr("Table_Name").ToString(), dt)
                    fnInsertLog(dr("Table_Name").ToString() + "_Log", dt)
                    Sql.CommitTran()
                    'End If
                Catch ex As Exception
                    Sql.RollBack()
                End Try

            ElseIf isTableCreated = "Insert" Then
                Try

                    'If (dr("Table_Name").ToString() = "ET_VEND_OTHERS" Or dr("Table_Name").ToString() = "ET_PO_HEADER" Or dr("Table_Name").ToString() = "tbl_Sync_Material") Then
                    '    Sql.BeginTran()
                    '    fnDelete(dr("Table_Name").ToString())
                    '    fnInsert(dr("Table_Name").ToString(), dt)
                    '    fnInsertLog(dr("Table_Name").ToString() + "_Log", dt)

                    '    Sql.CommitTran()
                    'Else
                    Sql.BeginTran()
                    'If (dr("Table_Name").ToString() = "ET_VEND_DETAIL") Then
                    '    fnInsertAdmin(dr("Table_Name").ToString())
                    '    fnInsertVendorCode(dr("Table_Name").ToString(), dt)
                    'End If
                    fnDelete(dr("Table_Name").ToString())
                    fnInsert(dr("Table_Name").ToString(), dt)
                    fnInsertLog(dr("Table_Name").ToString() + "_Log", dt)
                    Sql.CommitTran()
                    'End If

                Catch ex As Exception
                    Sql.RollBack()
                End Try

            End If
            'eCell = Nothing
            'eSheet = Nothing
            'eBook = Nothing
            'eApp.Quit()
            ''eApp = Nothing


        Catch ex As Exception
            CommonMethods.fnWriteErrorLog("fnLoadExcelDetails1", ex.Message)
        Finally
            ' eBook.Close()
            eCell = Nothing
            eSheet = Nothing
            eBook = Nothing
            eApp.Quit()
            eApp = Nothing
        End Try

    End Sub

    Private Function fnChangeStringFormat(fileRefrence As String, dtMerge As DataTable) As DataTable
        Try
            Dim fileref As String = CommonMethods.fnGetFileReference()
            Dim file() As String = fileref.Split(",")
            For i As Integer = 0 To file.Length - 1
                If (fileRefrence.Contains(file(i))) Then
                    Dim queryFormat As String = "select * from tbl_Sync_Excel_Sheet_Mapping where Exel_Reference='" & DBNulls.StringValue(file(i)) & "' And (DataType='" & CommonVariables.stringDate & "' Or DataType='" & CommonVariables.stringDecimal & "')"
                    Dim dtFormat As DataTable = New DataTable()
                    dtFormat = Sql.getDatatable(queryFormat)

                    For Each singleItem As DataRow In dtFormat.Rows
                        Dim ColumnName = DBNulls.StringValue(singleItem("Exel_Column"))
                        For Each drMerger As DataRow In dtMerge.Rows
                            Try
                                If DBNulls.StringValue(singleItem("DataType")) = CommonVariables.stringDate Then
                                    If (DBNulls.StringValue(drMerger(ColumnName)) <> String.Empty) Then
                                        drMerger(ColumnName) = String.Format("{0:" + DBNulls.StringValue(singleItem("Expected_Format")) + "}", Convert.ToDateTime(drMerger(ColumnName)))
                                    Else
                                        drMerger(ColumnName) = String.Format("{0:" + DBNulls.StringValue(singleItem("Expected_Format")) + "}", Convert.ToDateTime(CommonVariables.defaultDate))
                                    End If
                                    'ElseIf DBNulls.StringValue(singleItem("DataType")) = CommonVariables.stringDecimal Then
                                    '    If (DBNulls.StringValue(drMerger(ColumnName)) = String.Empty) Then
                                    '        drMerger(ColumnName) = DBNulls.NumberValue("0")
                                    '    End If
                                End If
                            Catch ex As Exception
                                CommonMethods.fnWriteErrorLog("fnChangeStringFormat() : DateTime not in format", ex.Message)
                                drMerger(ColumnName) = String.Format("{0:" + DBNulls.StringValue(singleItem("Expected_Format")) + "}", Convert.ToDateTime(CommonVariables.defaultDate))
                            End Try
                        Next
                    Next
                    Return dtMerge

                End If
            Next
            'Return dtMerge  'return Code written by Pawan
        Catch ex As Exception
            CommonMethods.fnWriteErrorLog("fnChangeStringFormat() : Failed to change format for Table", ex.Message)

        End Try
    End Function

    Private Function fnTableCreate(TableName As String, NoOfColumns As Integer, FileName As String, dtColumns As DataTable) As String
        Try

            Dim str() As String = columnstring.Split(";")

            Dim query As String = "Select * from INFORMATION_SCHEMA.TABLES where TABLE_NAME = '" & TableName & "'"
            Dim dt As DataTable = Sql.getDatatable(query)
            If dt.Rows.Count <= 0 Then
                If (NoOfColumns <= dtColumns.Columns.Count - 1) Then

                    query = "Create Table " & TableName & "("
                    For i As Integer = 0 To str.Length - 1
                        If (i <> str.Length - 1) Then
                            query += str(i) + ","
                        Else
                            query += str(i)
                        End If

                    Next
                    query += ");"

                    Try
                        Sql.ExecuteSql(query)
                        Return True
                    Catch ex As Exception
                        Return False
                    End Try
                Else
                    CommonMethods.fnWriteErrorLog("Failed to Load Excel data '" & FileName.ToString() & "' into Table '" & TableName.ToString() & "'", "Duplicate Columns Or Columns Less than No. of mandetory Columns")
                    Return False
                End If
            Else
                Return "Insert"
            End If
        Catch ex As Exception
            CommonMethods.fnWriteErrorLog("fnTableCreate() : Failed to create Table", ex.Message)
            Return False
        End Try

    End Function

    'Private Function fnInsertVendorCode(tableName As String, dt As DataTable)
    '    For Each dr As DataRow In dt.Rows
    '        Dim query As String = "Select * From ET_VEND_OTHERS Where Vend_Code='" + dr("Col1").ToString() + "' "
    '        Dim dt1 As DataTable = Sql.getDatatable(query)
    '        If dt1.Rows.Count <= 0 Then
    '            query = "Insert Into ET_VEND_OTHERS (Vend_Code,VendorPassword,IsApproved) values ('" + dr("Col1").ToString() + "','cABhAHMAcwB3AG8AcgBkAA==','N')"
    '            Sql.ExecuteSql(query)
    '        End If
    '    Next
    'End Function
    'Private Function fnInsertAdmin(tableName As String)
    '    Dim str As String = "ADMIN"
    '    Dim query As String = "Select * from ET_VEND_DETAIL where Col1 = '" + str.ToLower() + "'"
    '    Dim dt As DataTable = Sql.getDatatable(query)
    '    If dt.Rows.Count <= 0 Then
    '        Dim query1 = "INSERT INTO ET_VEND_DETAIL (PrimaryColumn,Col1) values ('admin','admin')"
    '        Sql.ExecuteSql(query1)
    '    End If

    '    query = "Select * from ET_VEND_OTHERS where Vend_Code = '" + str.ToLower() + "'"
    '    dt = Sql.getDatatable(query)
    '    If dt.Rows.Count <= 0 Then
    '        Dim query1 = "INSERT INTO ET_VEND_OTHERS (Vend_Code,VendorPassword,IsApproved) values ('admin','QQA=','Y')"
    '        Sql.ExecuteSql(query1)
    '    End If


    '    str = "VADMIN"
    '    query = "Select * from ET_VEND_DETAIL where Col1 = '" + str.ToLower() + "'"
    '    Dim dt1 As DataTable = Sql.getDatatable(query)
    '    If dt1.Rows.Count <= 0 Then
    '        Dim query1 = "INSERT INTO ET_VEND_DETAIL (PrimaryColumn,Col1) values ('vadmin','vadmin')"
    '        Sql.ExecuteSql(query1)
    '    End If

    '    query = "Select * from ET_VEND_OTHERS where Vend_Code = '" + str.ToLower() + "'"
    '    dt1 = Sql.getDatatable(query)
    '    If dt1.Rows.Count <= 0 Then
    '        Dim query1 = "INSERT INTO ET_VEND_OTHERS (Vend_Code,VendorPassword,IsApproved) values ('vadmin','QQA=','Y')"
    '        Sql.ExecuteSql(query1)
    '    End If
    'End Function

    Private Function fnDelete(tableName As String)
        Try
            Dim query = "DELETE FROM " & tableName & ""
            Sql.ExecuteSql(query)
            CommonMethods.fnWriteErrorLog("fnDelete() : Records deletion successfull.................", "deletion successfull")
        Catch ex As Exception
            CommonMethods.fnWriteErrorLog("fnDelete() : Records deletion failed...", "Deletion failed")
        End Try
    End Function

    Public Function fnSimpleMove(fileName As String)
        Try

            Dim myDate As String = ""
            myDate = CommonMethods.fnMyDateTimeRTC(DateTime.Now)
            Dim DestFile As String = "\\192.168.1.71\dashbord\dashbord\prd\Done\" + fileName + "_" + myDate + " "
            Dim sourceFile As String = "\\192.168.1.71\dashbord\dashbord\prd\" + fileName + " "

            'System.IO.File.Move(sourceFile, DestFile)
            System.IO.File.Copy(sourceFile, DestFile)

            CommonMethods.fnWriteErrorLog("fnMove() : Files has been moved successfull.................", "Moved Successfull")

            '// To move an entire directory. To programmatically modify or combine
            '// path strings, use the System.IO.Path class.
            'System.IO.Directory.Move(@"C:\Users\Public\public\test\", @"C:\Users\Public\private");
            'System.IO.Directory.Move(@"C:\Users\Public\public\test\", @"C:\Users\Public\private");
        Catch ex As Exception
            CommonMethods.fnWriteErrorLog("fnMove() : Files has not been moved successfull.................", "Moved Unsuccessfull")
        End Try
    End Function

    Private Function fnDeleteFromTable(dtTable As DataTable)
        Try

            For Each dr As DataRow In dtTable.Rows
                ''delete query
            Next
        Catch ex As Exception

        End Try
    End Function

    Private Function fnInsert(tableName As String, dtTable As DataTable)
        Try

            'fnDeleteFromTable(dtTable)

            Dim insertQuery = String.Empty
            fnSetProgressBar()
            For Each dr As DataRow In dtTable.Rows
                Dim strValues = String.Empty
                Dim insertColumn = String.Empty
                Dim insert()
                Dim query = "SELECT COUNT(COLUMN_NAME) As Number FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = '" & tableName & "'"
                Dim count As Integer = Sql.GetColumnValue(query)
                For Each dc As DataColumn In dtTable.Columns
                    insertColumn = insertColumn + dr(dc.ColumnName) + ";"
                    insert = insertColumn.Split(";")
                Next

                For i As Integer = 0 To count - 1
                    If i <> count - 1 Then 'count - 1
                        If i < insert.Length Then
                            strValues = strValues + "'" + insert(i) + "',"
                        Else
                            strValues = strValues + " ' ',"
                        End If

                    Else
                        If i < insert.Length Then
                            strValues = strValues + "'" + insert(i) + "'"
                        Else
                            strValues = strValues + "' '"
                        End If

                    End If
                    If insertQuery.Length > 3000 Then
                        Try
                            Sql.ExecuteSql(insertQuery)
                            insertQuery = String.Empty
                        Catch ex As Exception
                            CommonMethods.fnWriteErrorLog("fnInsert() : Failed to Execute the bulk insert query", ex.Message)
                            Dim duplicateException() As String = ex.ToString().Split(New String() {"Duplicate key was ignored."}, StringSplitOptions.None)
                            duplicateCounter = duplicateException.Length - 1
                        End Try
                    End If
                Next

                insertQuery = insertQuery + "insert into " & tableName & " values(" & strValues & ");"
                'successfullCount = successfullCount + 1
                TotalCount = TotalCount + 1

            Next


            Try
                fnSetProgressBar()
                Sql.ExecuteSql(insertQuery)

                CommonMethods.fnWriteErrorLog("fnInsert() : Records Inserted successfull.................", "Insertion successfull")


            Catch ex As Exception
                CommonMethods.fnWriteErrorLog("fnInsert() : Failed to Execute the bulk insert query", ex.Message)
                Dim duplicateException() As String = ex.ToString().Split(New String() {"Duplicate key was ignored."}, StringSplitOptions.None)
                duplicateCounter = duplicateException.Length - 1
            End Try
            successfullCount = TotalCount - duplicateCounter
        Catch ex As Exception
            CommonMethods.fnWriteErrorLog("fnInsert() : Failed to Insert", ex.Message)
        End Try
    End Function

    Private Function fnInsertLog(tableName As String, dtTable As DataTable)
        Try

            Dim insertQuery = String.Empty
            fnSetProgressBar()
            For Each dr As DataRow In dtTable.Rows
                Dim strValues = String.Empty
                Dim insertColumn = String.Empty
                Dim insert()
                Dim query = "SELECT COUNT(COLUMN_NAME) As Number FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = '" & tableName & "'"
                Dim count As Integer = Sql.GetColumnValue(query)

                'insertColumn = "DateTime;"
                'dtTable.Columns.Add("DateTime", GetType(System.String)).SetOrdinal(0)
                For Each dc As DataColumn In dtTable.Columns

                    insertColumn = insertColumn + dr(dc.ColumnName) + ";"
                    insert = insertColumn.Split(";")

                Next


                For i As Integer = 0 To count - 1
                    If i <> count - 1 Then 'count - 1
                        If i < insert.Length Then
                            strValues = strValues + "'" + insert(i) + "',"
                        Else
                            strValues = strValues + " ' ',"
                        End If

                    Else
                        If i < insert.Length Then
                            strValues = strValues + "'" + insert(i) + "'"
                        Else
                            strValues = strValues + "' '"
                        End If

                    End If
                Next

                strValues = strValues + "'" + DBNulls.StringValue(DateTime.Now) + "'"

                insertQuery = insertQuery + "insert into " & tableName & " values(" & strValues & ");"
                '  successfullCount = successfullCount + 1
                TotalCount = TotalCount + 1


                If insertQuery.Length > 4500 Then
                    Try
                        Sql.ExecuteSql(insertQuery)
                        insertQuery = String.Empty
                    Catch ex As Exception

                    End Try
                End If
            Next


            Try
                fnSetProgressBar()
                Sql.ExecuteSql(insertQuery)

                CommonMethods.fnWriteErrorLog("fnInsertLog() : Records Inserted successfull.................", "Insertion successfull")


            Catch ex As Exception
                CommonMethods.fnWriteErrorLog("fnInsertLog() : Failed to Execute the bulk insert query", ex.Message)
                Dim duplicateException() As String = ex.ToString().Split(New String() {"Duplicate key was ignored."}, StringSplitOptions.None)
                duplicateCounter = duplicateException.Length - 1
            End Try
            successfullCount = TotalCount - duplicateCounter
        Catch ex As Exception
            CommonMethods.fnWriteErrorLog("fnInsertLog() : Failed to Insert", ex.Message)
        End Try

    End Function

    Private Function fnMergeData(dtMerge As DataTable, NoofColumn As String, ByVal tablename As String) As DataTable
        Try
            Dim dt As DataTable = New DataTable()
            ' Dim i As Integer = 0
            Dim rowIndex As Integer = 0
            Dim rowIndex1 As Integer = 0
            Dim colIndex As Integer = 0
            Dim dateTime As Date = System.DateTime.Now()

            Dim coulmncount As Integer = 0
            If dtMerge.Columns.Count - 1 > NoofColumn Then
                coulmncount = NoofColumn
            Else
                coulmncount = dtMerge.Columns.Count - 1
            End If

            For Each singleItem As DataRow In dtMerge.Rows
                For i As Integer = 0 To coulmncount
                    If Not singleItem(i).ToString() = String.Empty Then
                        If singleItem(i) = CommonVariables.ColumnStart Then
                            'dt.Columns.Add(singleItem(i))
                            rowIndex = dtMerge.Rows.IndexOf(singleItem)

                            rowIndex1 = rowIndex

                            colIndex = i

                            Exit For
                        End If
                        '   i = i + 1
                    End If
                Next
            Next

            dt.Columns.Clear()
            dt.Columns.Add("PrimaryColumn")
            'dt.Columns.Add("DateT")
            columnstring = "PrimaryColumn nvarchar(50) ;"
            'columnstring = "DateT date"
            Dim count As Integer = 1
            'For j As Integer = 0 To dtMerge.Columns.Count - 1

            ' Dim dr As DataRow = dtMerge.Rows(rowIndex)

            ' Dim dc As DataColumnCollection = dt.Columns
            Dim colcount As Integer = 1
            For Each dc As DataColumn In dtMerge.Columns
                If colcount <= NoofColumn Then
                    dt.Columns.Add("Col" + colcount.ToString())
                    columnstring += "[" + "Col" + colcount.ToString() + "] nvarchar(100);"
                    colcount = colcount + 1
                End If
            Next
            '    If rowIndex1 = rowIndex Then

            '    If (dr(j).ToString() <> String.Empty) Then
            '        If Not dc.Contains(dr(j)) Then
            '            dt.Columns.Add(dr(j))
            '            columnstring += "[" + dr(j) + "] nvarchar(MAX);"
            '        Else
            '            dt.Columns.Add(dr(j) + count.ToString())
            '            columnstring += "[" + dr(j) + count.ToString() + "] nvarchar(MAX);"
            '            count = count + 1
            '        End If
            '    End If
            'End If


            'Next


            For j As Integer = rowIndex + 1 To dtMerge.Rows.Count - 1
                Dim PrimaryCloumn As String = String.Empty
                Dim DateTim As Date = System.DateTime.Now
                Dim singleItem As DataRow = dt.NewRow
                Dim dr As DataRow = dtMerge.Rows(rowIndex + 1)
                Dim forcolumncount As Integer = dt.Columns.Count - 1
                'If dtMerge.Columns.Count - 1 > dt.Columns.Count - 1 Then
                '    forcolumncount = dtMerge.Columns.Count - 1
                'ElseIf dtMerge.Columns.Count - 1 < dt.Columns.Count - 1 Then
                '    forcolumncount = dtMerge
                'Else
                '    forcolumncount = dt.Columns.Count - 1
                'End If

                'If tablename = "tblSalesDetTemplet" Then
                '    coulmncount = 10
                'End If


                'If tablename = "tbl_Sync_Price" Then
                '    For k As Integer = colIndex To dtMerge.Columns.Count - 1
                '        If Not k >= NoofColumn + 1 Then
                '            If (dr(k).ToString() <> String.Empty) Then
                '                Dim strcoulumnName = String.Empty
                '                If k <> NoofColumn Then
                '                    strcoulumnName = dt.Columns(k - (colIndex - 1)).ToString()
                '                Else
                '                    strcoulumnName = dt.Columns(k).ToString()
                '                End If

                '                If (strcoulumnName <> String.Empty) Then
                '                    If dr(k).ToString().Contains("'") Or dr(k).ToString().Contains("""") Or dr(k).ToString().Contains("#") Then
                '                        dr(k) = dr(k).ToString().Replace("'", " ").Replace(ControlChars.Quote, " ").Replace("#", " ")
                '                    End If
                '                    singleItem(strcoulumnName) = dr(k)
                '                    If (Not k > coulmncount) Then
                '                        If (strcoulumnName = "Col3" Or strcoulumnName = "Col4" Or strcoulumnName = "Col5" Or strcoulumnName = "Col6") Then
                '                            PrimaryCloumn += singleItem(strcoulumnName)
                '                        End If
                '                    End If
                '                End If
                '            End If
                '        End If

                '    Next

                'Else

                For k As Integer = colIndex To dtMerge.Columns.Count - 1
                    If Not k >= NoofColumn + 1 Then
                        If (dr(k).ToString() <> String.Empty) Then
                            Dim strcoulumnName = String.Empty
                            If k <> NoofColumn Then
                                strcoulumnName = dt.Columns(k - (colIndex - 1)).ToString()
                            Else
                                strcoulumnName = dt.Columns(k).ToString()
                            End If

                            If (strcoulumnName <> String.Empty) Then
                                If dr(k).ToString().Contains("'") Or dr(k).ToString().Contains("""") Or dr(k).ToString().Contains("#") Then
                                    dr(k) = dr(k).ToString().Replace("'", " ").Replace(ControlChars.Quote, " ").Replace("#", " ")
                                End If
                                singleItem(strcoulumnName) = dr(k)
                                If (Not k > coulmncount) Then

                                    PrimaryCloumn += singleItem(strcoulumnName)
                                End If
                            End If
                        End If
                    End If

                Next
                'End If

                'PrimaryCloumn = Now



                singleItem("PrimaryColumn") = Now
                If (singleItem("PrimaryColumn").ToString() <> String.Empty) Then
                    dt.Rows.Add(singleItem)
                End If
                'singleItem("DateT") = DateTim
                'If (singleItem("DateT").ToString() <> String.Empty) Then
                '    dt.Rows.Add(singleItem)
                'End If
                rowIndex = rowIndex + 1
            Next
            fnSetProgressBar()

            Return dt

        Catch ex As Exception
            CommonMethods.fnWriteErrorLog("fnMergeData() : Failed to convert excel data into DataTable", ex.Message)
        End Try
    End Function

    Private Function fnArrangeFileName(fileName As String) As String
        Try
            Dim file() As String = fileName.Split(".")
            Dim f As String = file(0)
            If CommonMethods.fnGetFlagforSync.ToLower() = False Then
                f = f + "_" + DBNulls.StringValue(DateTime.Now.Day.ToString("00")) + "_" + DBNulls.StringValue(DateTime.Now.Month.ToString("00")) + "_" + DBNulls.StringValue(DateTime.Now.Year) + "." + file(1)
            Else
                f = f + "_" + CommonMethods.fnGetDateForFileSync() + "." + file(1)
            End If
            Return f
        Catch ex As Exception
            CommonMethods.fnWriteErrorLog("fnArrangeFileName() : Failed to re-arrange file name", ex.Message)
        End Try

    End Function

    Public Function fnSetProgressBar()
        increment = increment + 10
        If increment > ProgressBar1.Maximum Then
            increment = ProgressBar1.Maximum
        End If
        ProgressBar1.Value = increment
    End Function

    Public Function fnSetEmailNotification()
        Try

            Dim query = "Select * from tbl_SMTP_Settings"
            Dim dr As DataRow = Sql.getDataRow(query)

            Dim EmailTo As String = CommonMethods.fnGetEmailNotification()

            query = "Select * from tbl_Email_formats where Email_Code='Excel_SyncNotification'"
            Dim dr1 As DataRow = Sql.getDataRow(query)

            CommonMethods.fnWriteErrorLog("sendEmail()", "Email Notification Start")
            Dim IsMailSent = sendEmail(DBNulls.StringValue(dr("SMTP_Host")), DBNulls.StringValue(dr("SMTP_Port")), DBNulls.StringValue(dr("Email_Id")), DBNulls.StringValue(dr("Email_Password")), DBNulls.StringValue(EmailTo), DBNulls.StringValue(dr1("Email_Body")), DBNulls.StringValue(dr1("Email_Subject")), DBNulls.StringValue("Excel Sync Notification"))
            If IsMailSent = True Then
                CommonMethods.fnWriteErrorLog("sendEmail()", "Email Send Successfully...")
                CommonMethods.fnWriteErrorLog("sendEmail()", "Email Notification End")
            Else
                CommonMethods.fnWriteErrorLog("sendEmail()", "Email Sending fail...")
                CommonMethods.fnWriteErrorLog("sendEmail()", "Email Notification End")
            End If

        Catch ex As Exception
            CommonMethods.fnWriteErrorLog("fnSetEmailNotification()", ex.Message)
        End Try
    End Function

    Public Function sendEmail(serverName As String, portNo As Integer, emailId As String, password As String, [to] As String, body As String, _
        subject As String, displayName As String) As Boolean
        Try

            Dim mail As New System.Net.Mail.MailMessage()
            Dim smtp As New SmtpClient(serverName, portNo)
            smtp.DeliveryMethod = SmtpDeliveryMethod.Network

            smtp.EnableSsl = False

            smtp.EnableSsl = True
            smtp.Credentials = New System.Net.NetworkCredential(emailId, password)

            ' If txtUsePass.Checked = True Then
            'smtp.EnableSsl = True
            'smtp.Credentials = New System.Net.NetworkCredential(emailId, password)
            'Else
            'smtp.UseDefaultCredentials = False
            'End If

            mail.From = New MailAddress(emailId, displayName)
            Dim toEmails As String() = [to].Split(","c)
            If toEmails.Length > 0 Then
                For Each singleEmail As String In toEmails
                    mail.[To].Add(singleEmail)
                Next
            Else
                mail.[To].Add([to])
            End If

            mail.Subject = subject

            mail.IsBodyHtml = True
            mail.Body = body

            Dim attachment As System.Net.Mail.Attachment
            attachment = New System.Net.Mail.Attachment(Application.StartupPath & "\EmailLogs" & "\" & CommonMethods.fnDateTimeRTC(DateTime.Now) & ".txt")
            mail.Attachments.Add(attachment)

            smtp.Send(mail)

            Return True
        Catch ie As InvalidOperationException
            If Not ie.InnerException Is Nothing Then
                CommonMethods.fnWriteErrorLog("sendEmail()", ie.Message)
            Else
                CommonMethods.fnWriteErrorLog("sendEmail()", ie.Message)
            End If
            Return False
            'throw ie;

        Catch sre As SmtpFailedRecipientsException
            If Not sre.InnerException Is Nothing Then
                CommonMethods.fnWriteErrorLog("sendEmail()", sre.Message)
            Else
                CommonMethods.fnWriteErrorLog("sendEmail()", sre.Message)
            End If
            Return False
            'throw sre;

        Catch se As SmtpException
            If Not se.InnerException Is Nothing Then
                CommonMethods.fnWriteErrorLog("sendEmail()", se.Message)
            Else
                CommonMethods.fnWriteErrorLog("sendEmail()", se.Message)
            End If
            Return False
            'throw se;
        Catch ex As Exception
            If Not ex.InnerException Is Nothing Then
                CommonMethods.fnWriteErrorLog("sendEmail()", ex.Message)
            Else
                CommonMethods.fnWriteErrorLog("sendEmail()", ex.Message)
            End If

            Return False
            'throw ex;

        End Try

    End Function
End Class