﻿Imports vtSqlConnection.vtWindows
Imports System.IO
Imports System.Xml
Imports System.Configuration
Imports Telerik.WinControls.UI
Imports System.Net.NetworkInformation
Imports System.Management

Public Class CommonMethods
    Shared Function fnGetFilePath() As String
        Try
            Dim path As String = String.Empty
            Dim xmldoc As String = String.Empty
            Dim ans As String = String.Empty
            path = Application.StartupPath & "\Config\GabrielConfig.xml"
            xmldoc = File.ReadAllText(path)
            Dim reader As XmlReader = XmlReader.Create(New StringReader(xmldoc))

            Try
                reader.ReadToFollowing("Settings")
                reader.MoveToNextAttribute()

                reader.ReadToFollowing("filepath")
                ans = reader.ReadElementContentAsString()
                Return ans

            Finally
                If reader IsNot Nothing Then
                    reader.Close()
                End If
            End Try

        Catch ex As Exception
            CommonMethods.fnWriteErrorLog("fnGetFilePath() : Reading of Excel data Failed", ex.Message)
        End Try

    End Function

    Shared Function fnGetEmailNotification() As String
        Try
            Dim path As String = String.Empty
            Dim xmldoc As String = String.Empty
            Dim ans As String = String.Empty
            path = Application.StartupPath & "\Config\GabrielConfig.xml"
            xmldoc = File.ReadAllText(path)
            Dim reader As XmlReader = XmlReader.Create(New StringReader(xmldoc))

            Try
                reader.ReadToFollowing("Settings")
                reader.MoveToNextAttribute()

                reader.ReadToFollowing("EmailNotificationTo")
                ans = reader.ReadElementContentAsString()
                Return ans

            Finally
                If reader IsNot Nothing Then
                    reader.Close()
                End If
            End Try

        Catch ex As Exception
            CommonMethods.fnWriteErrorLog("fnGetFilePath() : Reading of Excel data Failed", ex.Message)
        End Try

    End Function

    Public Shared Function getUniqueColumnID(ByVal tableName As String, ByVal columnName As String, ByVal appendName As String) As String
        Try
        Dim str As String = String.Empty
        Dim num As String = String.Empty
        Dim tmpStr As String = String.Empty
        Dim format As String = "00000000"
        Dim nos As Integer = 0
        Dim count As Integer = 0
        Dim query As String = String.Empty
        query = "SELECT MAX(" & columnName & ") FROM " & tableName
        str = Sql.GetColumnValue(query)
        If str Is String.Empty Then
            Return appendName & "-00000001"
        Else
            If str.Contains("-") Then
                num = DirectCast(str.Split("-"), String())(1)
                nos = Convert.ToInt32(num) + 1
                tmpStr = nos
                count = (tmpStr.Count())
                count = 8 - count
                While count > 0
                    tmpStr = "0" & tmpStr
                    count = count - 1
                End While

                Return appendName & "-" & tmpStr
            End If
            End If
        Catch ex As Exception
            CommonMethods.fnWriteErrorLog("getUniqueColumnID()", ex.Message)
        End Try
    End Function

    Public Shared Sub fnWriteErrorLog(ByVal FunctionSource As String, ByVal errDescription As String)
        Try
            Dim errDescription_DB As String = String.Empty
            errDescription_DB = errDescription
            If DBNulls.StringValue(errDescription.Contains("|")) Then
                errDescription = DBNulls.StringValue(errDescription.Replace("|", vbNewLine))
            End If
            Dim fW As StreamWriter
            Dim ErroLogFile As String
            If Not IO.Directory.Exists(Application.StartupPath & "\ErrorLogs") Then
                IO.Directory.CreateDirectory(Application.StartupPath & "\ErrorLogs")
            End If
            ErroLogFile = Application.StartupPath & "\ErrorLogs" & "\" & fnDateTimeRTC(DateTime.Now) & ".txt"
            fW = New StreamWriter(ErroLogFile, True)
            fW.WriteLine(" Source : " & FunctionSource)
            fW.WriteLine(" Date : " & Now.ToString("dd/MMM/yyyy HH:mm"))
            ' ''fW.WriteLine(" Error  : " & "")
            fW.WriteLine(" Description  : " & errDescription)
            fW.WriteLine("-----------------------------------------------------------")
            fW.Close()
            'fnWriteErrorLogToDatabase(FunctionSource, errDescription_DB)
        Catch ex As Exception
            CommonMethods.fnWriteErrorLog("fnWriteErrorLog()", ex.Message)
        End Try
    End Sub

    Public Shared Sub fnWriteStartAndEndLog()
        Try

            Dim fW As StreamWriter
            Dim ErroLogFile As String
            If Not IO.Directory.Exists(Application.StartupPath & "\ErrorLogs") Then
                IO.Directory.CreateDirectory(Application.StartupPath & "\ErrorLogs")
            End If
            ErroLogFile = Application.StartupPath & "\ErrorLogs" & "\" & fnDateTimeRTC(DateTime.Now) & ".txt"
            fW = New StreamWriter(ErroLogFile, True)
            fW.WriteLine("==========================================================")
            fW.Close()
            'fnWriteErrorLogToDatabase(FunctionSource, errDescription_DB)
        Catch ex As Exception
            CommonMethods.fnWriteErrorLog("fnWriteErrorLog()", ex.Message)
        End Try
    End Sub

    Shared Function fnConnectionString() As String
        Try


            Dim path As String = String.Empty
            Dim xmldoc As String = String.Empty
            Dim ans As String = String.Empty
            path = Application.StartupPath & "\Config\GabrielConfig.xml"
            xmldoc = File.ReadAllText(path)
            Dim reader As XmlReader = XmlReader.Create(New StringReader(xmldoc))
            Try
                reader.ReadToFollowing("Settings")
                reader.MoveToNextAttribute()
                reader.ReadToFollowing("ConnectionString")
                ans = reader.ReadElementContentAsString()
                Return ans

            Finally
                If reader IsNot Nothing Then
                    reader.Close()
                End If
            End Try
        Catch ex As Exception
            CommonMethods.fnWriteErrorLog("Failed to connect to database. Connection String", ex.Message)
        End Try

    End Function

    Shared Function fnGetFileReference() As String
        Try
            Dim path As String = String.Empty
            Dim xmldoc As String = String.Empty
            Dim ans As String = String.Empty
            path = Application.StartupPath & "\Config\GabrielConfig.xml"
            xmldoc = File.ReadAllText(path)
            Dim reader As XmlReader = XmlReader.Create(New StringReader(xmldoc))

            Try
                reader.ReadToFollowing("Settings")
                reader.MoveToNextAttribute()

                reader.ReadToFollowing("filereference")
                ans = reader.ReadElementContentAsString()
                Return ans

            Finally
                If reader IsNot Nothing Then
                    reader.Close()
                End If
            End Try

        Catch ex As Exception
            CommonMethods.fnWriteErrorLog("fnGetFileReference() : Reading of Excel data Failed", ex.Message)
        End Try

    End Function

    'Shared Function fnMoveFile() As String
    '    Dim fileName As String
    '    Dim sourcePath As String
    '    Dim DestPath As String

    '    fileName = "sales_db.xls"
    '    sourcePath = "D:\Sanjeev\Sales Dashboard"
    '    DestPath = "D:\Sanjeev\Sales Dashboard\Docs"

    '    Dim sourceFile = System.IO.Path.Combine(sourcePath, fileName)
    '    Dim destFile = System.IO.Path.Combine(DestPath, fileName)

    '    If (System.IO.Directory.Exists(DestPath)) Then
    '        System.IO.Directory.CreateDirectory(DestPath)
    '    End If

    '    System.IO.File.Copy(sourceFile, destFile, True)
    'End Function
    '----------------------------Excel Not avialable-----------------------------------'
    ' Given a document name, inserts a new worksheet.
    'Public Sub InsertWorksheet(ByVal docName As String)
    '    ' Open the document for editing.
    '    Dim spreadSheet As SpreadsheetDocument = SpreadsheetDocument.Open(docName, True)

    '    Using (spreadSheet)
    '        ' Add a blank WorksheetPart.
    '        Dim newWorksheetPart As WorksheetPart = spreadSheet.WorkbookPart.AddNewPart(Of WorksheetPart)()
    '        newWorksheetPart.Worksheet = New Worksheet(New SheetData())
    '        ' newWorksheetPart.Worksheet.Save()

    '        Dim sheets As Sheets = spreadSheet.WorkbookPart.Workbook.GetFirstChild(Of Sheets)()
    '        Dim relationshipId As String = spreadSheet.WorkbookPart.GetIdOfPart(newWorksheetPart)

    '        ' Get a unique ID for the new worksheet.
    '        Dim sheetId As UInteger = 1
    '        If (sheets.Elements(Of Sheet).Count > 0) Then
    '            sheetId = sheets.Elements(Of Sheet).Select(Function(s) s.SheetId.Value).Max + 1
    '        End If

    '        ' Give the new worksheet a name.
    '        Dim sheetName As String = ("Sheet" + sheetId.ToString())

    '        ' Append the new worksheet and associate it with the workbook.
    '        Dim sheet As Sheet = New Sheet
    '        sheet.Id = relationshipId
    '        sheet.SheetId = sheetId
    '        sheet.Name = sheetName
    '        sheets.Append(sheet)
    '    End Using
    'End Sub

    Shared Function fnGetFlagforSync() As String
        Try
            Dim path As String = String.Empty
            Dim xmldoc As String = String.Empty
            Dim ans As String = String.Empty
            path = Application.StartupPath & "\Config\GabrielConfig.xml"
            xmldoc = File.ReadAllText(path)
            Dim reader As XmlReader = XmlReader.Create(New StringReader(xmldoc))

            Try
                reader.ReadToFollowing("Settings")
                reader.MoveToNextAttribute()

                reader.ReadToFollowing("syncforParticularDate")
                ans = reader.ReadElementContentAsString()
                Return ans

            Finally
                If reader IsNot Nothing Then
                    reader.Close()
                End If
            End Try

        Catch ex As Exception
            CommonMethods.fnWriteErrorLog("fnGetFlagforSync() : Reading of Excel data Failed", ex.Message)
        End Try

    End Function

    Shared Function fnGetDateForFileSync() As String
        Try
            Dim path As String = String.Empty
            Dim xmldoc As String = String.Empty
            Dim ans As String = String.Empty
            path = Application.StartupPath & "\Config\GabrielConfig.xml"
            xmldoc = File.ReadAllText(path)
            Dim reader As XmlReader = XmlReader.Create(New StringReader(xmldoc))

            Try
                reader.ReadToFollowing("Settings")
                reader.MoveToNextAttribute()

                reader.ReadToFollowing("DateForFileSync")
                ans = reader.ReadElementContentAsString()
                Return ans

            Finally
                If reader IsNot Nothing Then
                    reader.Close()
                End If
            End Try

        Catch ex As Exception
            CommonMethods.fnWriteErrorLog("fnGetDateForFileSync() : Reading of Excel data Failed", ex.Message)
        End Try

    End Function

    Public Shared Function fnDateTimeRTC(ByVal vDate As DateTime) As String
        Return Format(vDate, "yyyyMMddHHmmss")
    End Function

    Public Shared Function fnMyDateTimeRTC(ByVal vDate As DateTime) As String
        Return Format(vDate, "yyyyMMddHHmmss")
    End Function


    Public Shared Sub fnWriteEmailLog(ByVal ExcelFileName As String, ByVal SyncedSuccessfullCount As String, ByVal unSyncedSuccessfullCount As String, ByVal duplicateCounter As String)
        Try
            Dim fW As StreamWriter
            Dim ErroLogFile As String
            If Not IO.Directory.Exists(Application.StartupPath & "\EmailLogs") Then
                IO.Directory.CreateDirectory(Application.StartupPath & "\EmailLogs")
            End If
            ErroLogFile = Application.StartupPath & "\EmailLogs" & "\" & fnDateTimeRTC(DateTime.Now) & ".txt"
            fW = New StreamWriter(ErroLogFile, True)
            fW.WriteLine("========== LOG RECORD ==========")
            fW.WriteLine(" Date : " & Now.ToString("dd/MMM/yyyy HH:mm"))
            fW.WriteLine("-------" & ExcelFileName & "-------")
            fW.WriteLine("Total Sucessfull Synced Records :" & SyncedSuccessfullCount)
            fW.WriteLine("Duplicate Records :" & duplicateCounter)
            fW.WriteLine("Total UnSucessfull Synced Records :" & unSyncedSuccessfullCount)
            fW.WriteLine("========================================================")
            fW.Close()
            'fnWriteErrorLogToDatabase(FunctionSource, errDescription_DB)
        Catch ex As Exception
            CommonMethods.fnWriteErrorLog("fnWriteErrorLog()", ex.Message)
        End Try
    End Sub
End Class
